#include <filesystem>
#include "Eigen/Dense"
#include "gtest/gtest.h"
#include "../ARRCObjectronCore/ARRCObjectronCore/BoxUtil.h"
#include <opencv2/highgui.hpp>
using namespace ARRCObjectron;


std::vector<Eigen::Vector3f> make_sample_bbox(Eigen::Vector3f scale);
Eigen::Matrix4f MakeTransformMatrix(Eigen::Vector3f translate, Eigen::Vector3f eulerAngle);
std::vector<Eigen::Vector2f> ProjectBBox(Eigen::Matrix4f intrinsic, Eigen::Matrix4f rt, std::vector< Eigen::Vector3f> bbox);
std::vector<cv::Point> Convert2ScreenPoint(std::vector<Eigen::Vector2f> keypoints);

int Lift2DTo3D(
    const Eigen::Matrix4f& projection_matrix,
    const Eigen::Matrix<float, 8, 4, Eigen::RowMajor>& epnp_alpha,
    const std::vector<Eigen::Vector2f>& keypoints_2d,
    std::vector<Eigen::Vector3f>& keypoints_3d);

TEST(EPnPTest, EPnPTest) {

    int width = 640;
    int height = 480;
    
    Eigen::Matrix4f intrinsic;
    
    //intrinsic <<
    //    572.4114, 0, 325.2611, 0,
    //    0, 573.57043, 242.04899, 0,
    //    0, 0, 1, 0,
    //    0, 0, 0, 1;

    intrinsic <<
        572.4114/ width, 0, 0, 0,
        0, 573.57043/ height, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1;

    Eigen::Matrix4f pursuit_model;
    
    pursuit_model << 
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, -1.0, 0.0,
        0.0, 0.0, 0.0, 1.0;

    //pursuit_model.setIdentity();
    //Eigen::Matrix4f obj_local_transform;
    //obj_local_transform.setIdentity();

    Eigen::Matrix<float, 8, 4, Eigen::RowMajor> epnp_alpha;
    epnp_alpha 
        << 
        4.0f, -1.0f, -1.0f, -1.0f,
        2.0f, -1.0f, -1.0f, 1.0f, 
        2.0f, -1.0f, 1.0f, -1.0f, 
        0.0f, -1.0f, 1.0f, 1.0f, 
        2.0f, 1.0f, -1.0f, -1.0f,
        0.0f, 1.0f, -1.0f, 1.0f, 
        0.0f, 1.0f, 1.0f, -1.0f, 
        -2.0f, 1.0f, 1.0f, 1.0f;

    auto obj_transform_gt = MakeTransformMatrix(Eigen::Vector3f(0.5, -1.3, -15), Eigen::Vector3f(-20, 90, 42));
    auto bbox = make_sample_bbox(Eigen::Vector3f(1, 1, 1));

    std::vector<Eigen::Vector2f> keypoints2d_gt, keypoints2d_est;
    
    EXPECT_EQ(bbox.size(), 9);
    keypoints2d_gt = ProjectBBox(intrinsic, obj_transform_gt, bbox);

    // Lift 2D->3D
    std::vector<Eigen::Vector3f> bbox_extimated;
    int result = Lift2DTo3D(intrinsic, epnp_alpha, keypoints2d_gt, bbox_extimated);
    EXPECT_EQ(result, 0);
    // Estimate transformation
  

    // Decode estimated bbox.
    Box box("category");
    box.Fit(bbox_extimated);
    auto bbox_fitted = box.GetVertices();
    Eigen::Vector3f estimated_scale = box.GetScale();
    Eigen::Matrix4f transform_rt = box.GetTransformation();
    Eigen::Matrix4f transform_scale
        = Eigen::MatrixXf::Identity(4, 4).array().colwise() * estimated_scale.homogeneous().array();
    
    Eigen::Matrix4f obj_transform_estimate
        = pursuit_model * transform_rt * transform_scale;

    keypoints2d_est = ProjectBBox(intrinsic, obj_transform_estimate, bbox);

    // Evaluate
    double error_allow = 0.00001;
    EXPECT_EQ(keypoints2d_gt.size(), keypoints2d_est.size());
    for (int i = 0; i < keypoints2d_est.size(); i++)
    {
        double dist = (keypoints2d_gt[i] - keypoints2d_est[i]).norm();
        EXPECT_TRUE(dist < error_allow);
    }
    
    // Visualize Results.
    //cv::Mat img_gt(height, width, CV_8UC3);
    //cv::Mat img_esti = img_gt.clone();

    //auto screen_points_gt = Convert2ScreenPoint(keypoints2d_gt);
    //auto screen_points_est = Convert2ScreenPoint(keypoints2d_est);

    //DrawBoundingBox(img_gt, screen_points_gt, cv::Scalar(255, 0, 0), 1);
    //DrawBoundingBox(img_esti, screen_points_est, cv::Scalar(255, 0, 0), 1);
    //cv::imshow("gt", img_gt);
    //cv::imshow("esti", img_esti);
    //cv::waitKey(0);

}
std::vector<cv::Point> Convert2ScreenPoint(std::vector<Eigen::Vector2f> keypoints)
{
    std::vector<cv::Point> screen_points(keypoints.size());
    for (int i = 0; i < keypoints.size(); i++)
        screen_points[i] = cv::Point(keypoints[i].x()*640, keypoints[i].y()*480);

    return screen_points;
}
std::vector<Eigen::Vector2f> ProjectBBox(Eigen::Matrix4f intrinsic, Eigen::Matrix4f rt, std::vector< Eigen::Vector3f> bbox)
{
    std::vector<Eigen::Vector2f> keypoints;
    for (auto v : bbox)
    {
        Eigen::Vector4f point3d = v.homogeneous();
        point3d = intrinsic * rt * point3d;

        float proj_x = point3d.x() / point3d.z(); // [-1,1]
        float proj_y = point3d.y() / point3d.z(); // [-1,1]

        float kpt_x = (proj_x + 1) * 0.5; // [0,1]
        float kpt_y = 1 - (proj_y + 1) * 0.5; // [0,1], flip y-axis for screen coordinate.

        keypoints.push_back(Eigen::Vector2f(kpt_x, kpt_y));
    }
    return keypoints;
}


int Lift2DTo3D(
    const Eigen::Matrix4f& projection_matrix,
    const Eigen::Matrix<float, 8, 4, Eigen::RowMajor>& epnp_alpha,
    const std::vector<Eigen::Vector2f>& keypoints_2d,
    std::vector<Eigen::Vector3f>& keypoints_3d)
{
    const float fx = projection_matrix(0, 0);
    const float fy = projection_matrix(1, 1);
    const float cx = projection_matrix(0, 2);
    const float cy = projection_matrix(1, 2);

    Eigen::Matrix<float, 16, 12, Eigen::RowMajor> m =
        Eigen::Matrix<float, 16, 12, Eigen::RowMajor>::Zero(16, 12);

    for (int i = 0; i < 8; ++i) {
    //CHECK_EQ(9, annotation.keypoints_size());
        //if (portrait) {
        //    // swap x and y given that our image is in portrait orientation
        //    u = keypoint2d.y * 2 - 1;
        //    v = keypoint2d.x * 2 - 1;
        //}
        //else {
        //    u = keypoint2d.x * 2 - 1;
        //    v = 1 - keypoint2d.y * 2;  // (1 - keypoint2d.y()) * 2 - 1
        //}
        //float u = keypoints_2d[i+1].x()/320.f - 1.f;
        //float v = 1.f - keypoints_2d[i+1].y()/240.f;

        float u = keypoints_2d[i + 1].x() * 2 - 1;
        float v = 1 - keypoints_2d[i + 1].y() * 2;

        for (int j = 0; j < 4; ++j) {
            // For each of the 4 control points, formulate two rows of the
            // m matrix (two equations).

            float control_alpha = epnp_alpha(i, j);

            m(i * 2, j * 3) = fx * control_alpha;
            m(i * 2, j * 3 + 2) = (cx + u) * control_alpha;
            m(i * 2 + 1, j * 3 + 1) = fy * control_alpha;
            m(i * 2 + 1, j * 3 + 2) = (cy + v) * control_alpha;
        }
    }
    // This is a self adjoint matrix. Use SelfAdjointEigenSolver for a fast
    // and stable solution.
    Eigen::Matrix<float, 12, 12, Eigen::RowMajor> mt_m = m.transpose() * m;
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix<float, 12, 12, Eigen::RowMajor>>
        eigen_solver(mt_m);
    if (eigen_solver.info() != Eigen::Success) {
        //return absl::AbortedError("Eigen decomposition failed.");
        assert("Eigen decomposition failed.");
        return -1;
    }
    //CHECK_EQ(12, eigen_solver.eigenvalues().size());
    // Eigenvalues are sorted in increasing order for SelfAdjointEigenSolver
    // only! If you use other Eigen Solvers, it's not guaranteed to be in
    // increasing order. Here, we just take the eigen vector corresponding
    // to first/smallest eigen value, since we used SelfAdjointEigenSolver.
    Eigen::VectorXf eigen_vec = eigen_solver.eigenvectors().col(0);
    Eigen::Map<Eigen::Matrix<float, 4, 3, Eigen::RowMajor>> control_matrix(
        eigen_vec.data());
    if (control_matrix(0, 2) > 0) {
        control_matrix = -control_matrix;
    }

    // First set the center keypoint.
    float x3d = control_matrix(0, 0);
    float y3d = control_matrix(0, 1);
    float z3d = control_matrix(0, 2);
    keypoints_3d.push_back(Eigen::Vector3f(x3d, y3d, z3d));

    Eigen::Matrix<float, 8, 3, Eigen::RowMajor> vertices =
        epnp_alpha * control_matrix;

    // Then set the 8 vertices.
    for (int i = 0; i < 8; i++)
    {
        float x3d = vertices(i, 0);
        float y3d = vertices(i, 1);
        float z3d = vertices(i, 2);
        keypoints_3d.push_back(Eigen::Vector3f(x3d, y3d, z3d));
    }
    return 0;
}

std::vector<Eigen::Vector3f> make_sample_bbox(Eigen::Vector3f scale)
{
    auto w = scale.x() / 2.f;
    auto h = scale.y() / 2.f;
    auto d = scale.z() / 2.f;

    std::vector<Eigen::Vector3f> bbox(9);
    // Define the local coordinate system, w.r.t. the center of the boxs
    bbox[0] << 0.00001f, 0.00001f, 0.00001f;
    bbox[1] << -w, -h, -d;
    bbox[2] << -w, -h, +d;
    bbox[3] << -w, +h, -d;
    bbox[4] << -w, +h, +d;
    bbox[5] << +w, -h, -d;
    bbox[6] << +w, -h, +d;
    bbox[7] << +w, +h, -d;
    bbox[8] << +w, +h, +d;

    return bbox;
}

Eigen::Matrix4f MakeTransformMatrix(Eigen::Vector3f translate, Eigen::Vector3f eulerAngle)
{
    const double PI = 3.1415926535897932;
    float pitch = PI / 180 * eulerAngle.x();
    float yaw = PI / 180 * eulerAngle.y();
    float roll = PI / 180 * eulerAngle.z();
    

    Eigen::AngleAxisf pitchAngle(pitch, Eigen::Vector3f::UnitX());
    Eigen::AngleAxisf yawAngle(yaw, Eigen::Vector3f::UnitY());
    Eigen::AngleAxisf rollAngle(roll, Eigen::Vector3f::UnitZ());
    
    
    
    

    Eigen::Quaternion<float> q = rollAngle * yawAngle * pitchAngle;

    //Eigen::Matrix3f rotationMatrix = q.matrix();
    Eigen::Matrix4f rotationMatrix;
    rotationMatrix.setIdentity();

    rotationMatrix.block<3, 3>(0, 0) = q.matrix();
    rotationMatrix.block<3, 1>(0, 3) = translate;

 /*   Eigen::Matrix4f output;
    output.setIdentity()*/;
    
    //rotationMatrix(0, 3) = translate.x();
    //rotationMatrix(1, 3) = translate.y();
    //rotationMatrix(2, 3) = translate.z();

    //Eigen::Vector4f temp = output.row(1);
    //output.row(1) = output.row(2);
    //output.row(2) = temp;

    //temp = output.col(1);
    //output.col(1) = output.col(2);
    //output.col(2) = temp;
    
    //make to r.h.c.
    //output.row(1) = -output.row(1);
    //output.row(2) = -output.row(2);

    return rotationMatrix;

}