#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <filesystem>
#include "gtest/gtest.h"
#include "../ARRCObjectronCore/Utils/CVUtils.h"
#include <fstream>
#include "TestUtils.cpp"

TEST(CVUtilTest, TransformImageTest) {

	const std::string filepath = TEST_RESOURCE_DIR + std::string("/BOP/lm/test/000002/rgb/000000.png");
	cv::Mat img = cv::imread(std::string(filepath));

	EXPECT_TRUE(true);
	EXPECT_EQ(img.cols, 640);
	EXPECT_EQ(img.rows, 480);
	
}

