#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <algorithm>

#include "gtest/gtest.h"
#include "../ARRCObjectronCore/OnnxRuntime/OnnxRuntime.h"
#include "../ARRCObjectronCore/OnnxRuntime/OnnxRuntimeUtils.h"
#include "../ARRCObjectronCore/Utils/CVUtils.h"
#include "../ARRCObjectronCore//ARRCObjectronCore/decoder.h"

using namespace ARRCObjectron;

class ObjectronTest : public::testing::Test
{
protected:

    void SetUp() override
    {
        const std::string modelpath = TEST_RESOURCE_DIR + std::string("/dnn_models/lm_obj2_test_ckp_32_train.onnx");
        const std::string imagepath = TEST_RESOURCE_DIR + std::string("/BOP/lm/test/000002/rgb/000000.png");

        cv::Mat image = cv::imread(std::string(imagepath));

        cv::Mat img_norm;
        image.convertTo(img_norm, CV_32F, 2.0 / 255, -1.);

        OnnxRuntime onnx;
        onnx.LoadModel(modelpath.c_str());
        Ort::Value tensor = CreateCHWTensor_float32(img_norm);
        std::vector<const Ort::Value*> input_tensors = { &tensor };

        auto output_tensors = onnx.Evaluate(input_tensors);
        ReleaseTensors(input_tensors); // //warning : the data of input tensors is manually allocated, so it is needed to release manually.

        heatmap = CreateMat_from_CHWTensor_float32(&output_tensors[0]);
        offsetmap = CreateMat_from_CHWTensor_float32(&output_tensors[1]);
       
    }
    cv::Mat heatmap;
    cv::Mat offsetmap;
};
TEST_F(ObjectronTest, EvaluationTest_objectron1) {
    
    //auto mask = Tensor2Mat(&output_tensors[2]);
    //cv::minMaxIdx(heatmap_tensor, &min, &max, &minIdx, &maxIdx);
    /*auto result = GetResultFromModel1();
    auto heatmap = result[0];
    auto offsetmap = result[1];*/

    int minIdx, maxIdx;
    double minVal, maxVal;
    cv::minMaxLoc(heatmap, &minVal, &maxVal);

    //EXPECT_EQ(maxIdx, 12);
    EXPECT_EQ(minVal, 0);
    EXPECT_DOUBLE_EQ(maxVal, 0.80096286535263062);

    //cv::Mat heatmap_colorMapJet = ConvertToHeatMap(heatmap);
    //const std::string save_output = TEST_RESOURCE_OUT_DIR + std::string("/heatmap.png");
    //cv::imwrite(save_output, heatmap_colorMapJet);
}

TEST_F(ObjectronTest, DecodingTest) {
    
    auto decoder = std::make_unique<Decoder>(
        BeliefDecoderConfig(BeliefDecoderConfig()));

    auto points = decoder->ExtractCenterKeypoints(heatmap);
    EXPECT_EQ(points.size(), 1);
    EXPECT_EQ(points[0].x, 20);
    EXPECT_EQ(points[0].y, 12);

    const float offset_scale = min(offsetmap.cols, offsetmap.rows);
    EXPECT_EQ(offset_scale, 30);

    std::vector<ARRCObjectron::Decoder::BeliefBox> boxes;
    ARRCObjectron::Decoder::BeliefBox box;

    auto center_point = points[0];

    box.box_2d.emplace_back(center_point.x, center_point.y);
    const int center_x = static_cast<int>(std::round(center_point.x));
    const int center_y = static_cast<int>(std::round(center_point.y));
    box.belief = heatmap.at<float>(center_y, center_x);
    
    decoder->DecodeByPeak(offsetmap, center_x, center_y, offset_scale, offset_scale, &box);

    if (decoder->IsNewBox(&boxes, &box)) {
        boxes.push_back(std::move(box));
    }

    const float x_scale = 1.0f / offsetmap.cols;
    const float y_scale = 1.0f / offsetmap.rows;
    std::shared_ptr<FrameAnnotation> frame_annotations = std::make_shared< FrameAnnotation>();
    for (const auto& box : boxes) {
        auto object = frame_annotations->add_annotations();
        for (const auto& point : box.box_2d) {
            auto point2d = object->add_keypoints()->mutable_point_2d();
            point2d->x = (point.first * x_scale);
            point2d->y = (point.second * y_scale);
        }
    }


    //box.box_2d[0]

    /*if (config_.voting_radius > 1) {
        DecodeByVoting(heatmap, offsetmap, center_x, center_y, offset_scale,
            offset_scale, &box);
    }
    else {
        DecodeByPeak(offsetmap, center_x, center_y, offset_scale, offset_scale,
            &box);
    }
    if (IsNewBox(&boxes, &box)) {
        boxes.push_back(std::move(box));
    }

    auto aa = tester->DecodeByPeak(points);*/

}

std::vector<cv::Mat> GetResultFromModel1()
{
    const std::string modelpath = TEST_RESOURCE_DIR + std::string("/dnn_models/lm_obj2_test_ckp_32_train.onnx");
    const std::string imagepath = TEST_RESOURCE_DIR + std::string("/BOP/lm/test/000002/rgb/000000.png");

    cv::Mat image = cv::imread(std::string(imagepath));
  
    cv::Mat img_norm;
    image.convertTo(img_norm, CV_32F, 2.0 / 255, -1.);

    OnnxRuntime onnx;
    onnx.LoadModel(modelpath.c_str());
    Ort::Value tensor = CreateCHWTensor_float32(img_norm);
    std::vector<const Ort::Value*> input_tensors = { &tensor };

    auto output_tensors = onnx.Evaluate(input_tensors);
    ReleaseTensors(input_tensors); // //warning : the data of input tensors is manually allocated, so it is needed to release manually.

    auto heatmap = CreateMat_from_CHWTensor_float32(&output_tensors[0]);
    auto offssetmap = CreateMat_from_CHWTensor_float32(&output_tensors[1]);

    std::vector<cv::Mat> result;
    result.push_back(heatmap);
    result.push_back(offssetmap);

    return result;
}

