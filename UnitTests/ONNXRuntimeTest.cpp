#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <algorithm>

#include "gtest/gtest.h"
#include "../ARRCObjectronCore/OnnxRuntime/OnnxRuntime.h"
#include "../ARRCObjectronCore/OnnxRuntime/OnnxRuntimeUtils.h"
#include "../ARRCObjectronCore/Utils/CVUtils.h"

TEST(ONNXRuntimeTest, LoadModelTest) {

    //my objectron model.
	const std::string modelpath = TEST_RESOURCE_DIR + std::string("/dnn_models/lm_obj2_test_ckp_32_train.onnx");
	OnnxRuntime onnx;
	onnx.LoadModel(modelpath.c_str());
    
    EXPECT_EQ(onnx.m_inputNodes.size(), 1);
    EXPECT_EQ(onnx.m_outputNodes.size(), 2);

    //mobilenet v2.
    const std::string modelpath_mobilenet = TEST_RESOURCE_DIR + std::string("/dnn_models/mobilenetv2-1.0.onnx");
    OnnxRuntime onnx_mobilenet;
    onnx_mobilenet.LoadModel(modelpath_mobilenet.c_str());

    EXPECT_EQ(onnx_mobilenet.m_inputNodes.size(), 1);
    EXPECT_EQ(onnx_mobilenet.m_outputNodes.size(), 1);

    //tiny yolo v2.
    const std::string modelpath_tiny_yolov2 = TEST_RESOURCE_DIR + std::string("/dnn_models/tiny_yolov2.onnx");
    OnnxRuntime onnx_tiny_yolov2;
    onnx_tiny_yolov2.LoadModel(modelpath_tiny_yolov2.c_str());

    EXPECT_EQ(onnx_tiny_yolov2.m_inputNodes.size(), 1);
    EXPECT_EQ(onnx_tiny_yolov2.m_outputNodes.size(), 1);
}
TEST(ONNXRuntimeUtilTest, TensorMatConversionTest) {
    
    const std::string imagepath = TEST_RESOURCE_DIR + std::string("/BOP/lm/test/000002/rgb/000000.png");
    cv::Mat image = cv::imread(std::string(imagepath));
    image.convertTo(image, CV_32F); // Convert [0,255] -> [0, 1.0f]

    Ort::Value tensor = CreateCHWTensor_float32(image); //warning : need to release tensor data manually.
    cv::Mat _image = CreateMat_from_CHWTensor_float32(&tensor);

    cv::Mat diff, diff_1c;
    cv::absdiff(image, _image, diff);

    cv::Mat splits[3];
    cv::split(diff, splits);
    EXPECT_EQ(cv::countNonZero(splits[0]), 0);
    EXPECT_EQ(cv::countNonZero(splits[1]), 0);
    EXPECT_EQ(cv::countNonZero(splits[2]), 0);

    ReleaseTensor(&tensor);
}

TEST(ONNXRuntimeTest, EvaluationTest_mobilenetV2) {

    const std::string modelpath = TEST_RESOURCE_DIR + std::string("/dnn_models/mobilenetv2-1.0.onnx");
    const std::string imagepath_sample1 = TEST_RESOURCE_DIR + std::string("/test_imgs/classification/cls_001.jpg");
  
    OnnxRuntime onnx;
    onnx.LoadModel(modelpath.c_str());
    const int rows = 224;
    const int cols = 224;

    // Test the sample 1.
    //
    cv::Mat img_sample1 = cv::imread(std::string(imagepath_sample1));

    cv::Mat img_resized;
    resize(img_sample1, img_resized, cv::Size(rows, cols));
    
    img_resized.convertTo(img_sample1, CV_32F, 1.0 / 255); // Convert [0,255] -> [0, 1.0f]

    // Normalization per channel
    // Normalization parameters obtained from 
    // https://github.com/onnx/models/tree/master/vision/classification/squeezenet
    cv::Mat img_norm = NormalizeUsingMeanStd(img_sample1, cv::Scalar(0.406, 0.456, 0.485), cv::Scalar(0.225, 0.224, 0.229));
    
    Ort::Value tensor1 = CreateCHWTensor_float32(img_norm);
    std::vector<const Ort::Value*> input_tensors1 = { &tensor1 };

    auto output_tensors = onnx.Evaluate(input_tensors1);
    ReleaseTensors(input_tensors1);
    
    auto info = output_tensors[0].GetTensorTypeAndShapeInfo();
    EXPECT_EQ(info.GetElementCount(), 1000);
    const float* tensorData = output_tensors[0].GetTensorData<float>();
    int index = std::distance(tensorData, std::max_element(tensorData, tensorData+1000));
    EXPECT_EQ(index, 363); ////363 is armadillo, see https://deeplearning.cms.waikato.ac.nz/user-guide/class-maps/IMAGENET/

    // Test the sample 2.
    //
    const std::string imagepath_sample2 = TEST_RESOURCE_DIR + std::string("/test_imgs/classification/cls_002.jpg");
    cv::Mat img_sample2 = cv::imread(std::string(imagepath_sample2));
    
    cv::Mat img_resized2;
    resize(img_sample2, img_resized2, cv::Size(rows, cols));

    img_resized2.convertTo(img_resized2, CV_32F, 1.0 / 255);

    cv::Mat img_norm2 = NormalizeUsingMeanStd(img_resized2, cv::Scalar(0.406, 0.456, 0.485), cv::Scalar(0.225, 0.224, 0.229));

    Ort::Value tensor2 = CreateCHWTensor_float32(img_norm2);
    
    std::vector<const Ort::Value*> input_tensors2= { &tensor2 };
    auto output_tensors2 = onnx.Evaluate(input_tensors2);
    ReleaseTensors(input_tensors2);
    
    auto info2 = output_tensors2[0].GetTensorTypeAndShapeInfo();
    EXPECT_EQ(info.GetElementCount(), 1000);
    const float* tensorData2 = output_tensors2[0].GetTensorData<float>();
    int index2 = std::distance(tensorData2, std::max_element(tensorData2, tensorData2 + 1000));
    EXPECT_EQ(index2, 470); ////470 is candle, taper, wax light, see https://deeplearning.cms.waikato.ac.nz/user-guide/class-maps/IMAGENET/
}
