//#include <filesystem>
//#include <string>
//using FilePath = std::filesystem::path; ///< FilePath
//
//static std::string GetFilePath(std::string filepath)
//{
//	std::replace(filepath.begin(), filepath.end(), '')
//
//}
#pragma once

#include <string>
#include <algorithm>
#include <sstream>
#include <utility>
#include <cstring>


static const char* const WSL_FILE_PATH_HEADER = "/mnt";
static const char WINDOWS_FILE_PATH_SEPARATOR = '\\';
static const char UNIX_FILE_PATH_SEPARATOR = '/';

static std::string windows_to_unix_file_path(std::string file_path, bool is_wsl) {
    // Replace the slashes
    std::replace(file_path.begin(), file_path.end(), WINDOWS_FILE_PATH_SEPARATOR, UNIX_FILE_PATH_SEPARATOR);

    // Convert the drive letter to lowercase
    std::transform(file_path.begin(), file_path.begin() + 1, file_path.begin(),
        [](unsigned char character) {
            return std::tolower(character);
        });

    // Remove the colon
    const auto drive_letter = file_path.substr(0, 1);
    const auto remaining_path = file_path.substr(2, file_path.size() - 2);
    file_path = drive_letter + remaining_path;

    std::stringstream stringstream;

    if (is_wsl) {
        stringstream << WSL_FILE_PATH_HEADER;
    }

    stringstream << "/";
    stringstream << file_path;

    return stringstream.str();
}