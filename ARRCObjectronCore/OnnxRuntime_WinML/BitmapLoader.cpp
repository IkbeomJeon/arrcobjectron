﻿#include "pch.h"
#include "BitmapLoader.h"
#if __has_include("BitmapLoader.g.cpp")
#include "BitmapLoader.g.cpp"
#endif

using namespace winrt;
using namespace Windows::UI::Xaml;

namespace winrt::ConsoleTest::implementation
{
    BitmapLoader::BitmapLoader()
    {
        InitializeComponent();
    }

    int32_t BitmapLoader::MyProperty()
    {
        throw hresult_not_implemented();
    }

    void BitmapLoader::MyProperty(int32_t /* value */)
    {
        throw hresult_not_implemented();
    }

    void BitmapLoader::ClickHandler(IInspectable const&, RoutedEventArgs const&)
    {
        Button().Content(box_value(L"Clicked"));
    }
}
