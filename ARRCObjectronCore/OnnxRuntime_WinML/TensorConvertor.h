#pragma once

#include <unknwn.h>
#include "winrt/Windows.Foundation.h"
#include <winrt/Windows.AI.MachineLearning.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.Graphics.Imaging.h>
#include <winrt/Windows.Media.h>
#include <winrt/Windows.Storage.h>
#include "winrt/Windows.Storage.Streams.h"

#include <string>
#include <fstream>

#include <Windows.h>

#pragma comment(lib, "d3d12.lib")


namespace TensorizationHelper
{
    //std::wstring GetFileName();
    winrt::Windows::AI::MachineLearning::TensorFloat SoftwareBitmapToSoftwareTensor2(
        winrt::Windows::Graphics::Imaging::SoftwareBitmap softwareBitmap);

    winrt::Windows::AI::MachineLearning::TensorFloat SoftwareBitmapToSoftwareTensor(
    winrt::Windows::Graphics::Imaging::SoftwareBitmap softwareBitmap);
  
    winrt::Windows::AI::MachineLearning::TensorFloat SoftwareBitmapToDX12Tensor(
    winrt::Windows::Graphics::Imaging::SoftwareBitmap softwareBitmap);
}