#pragma once

#include <winrt/Windows.Foundation.h>
#include <winrt/Windows.Storage.h>
#include <winrt/Windows.Storage.Streams.h>
#include <winrt/Windows.UI.Xaml.Media.Imaging.h>
#include <winrt/Windows.Graphics.Imaging.h>

namespace BitmapLoader
{
	using namespace winrt::Windows::Foundation;
	using namespace winrt::Windows::Storage;
	using namespace winrt::Windows::Storage::Streams;
	using namespace winrt::Windows::UI::Xaml::Media::Imaging;
	using namespace winrt::Windows::Graphics::Imaging;

	static IAsyncOperation<BitmapImage> GetBitmapImageFromURIasync(Uri uri)
	{
		RandomAccessStreamReference streamRef = RandomAccessStreamReference::CreateFromUri(uri);
		IRandomAccessStream stream{ co_await streamRef.OpenReadAsync() };
		BitmapImage bitmap{};
		bitmap.SetSource(stream);
		co_return bitmap;
	}
	static IAsyncOperation<BitmapImage> GetBitmapImageFromFileAsync(StorageFile file)
	{
		IRandomAccessStream stream{ co_await file.OpenAsync(FileAccessMode::Read) };
		BitmapImage bitmap{};
		bitmap.SetSource(stream);
		co_return bitmap;
	}

	static IAsyncOperation<SoftwareBitmap> GetSoftwareBitmapFromURIasync(Uri uri)
	{
		RandomAccessStreamReference streamRef = RandomAccessStreamReference::CreateFromUri(uri);
		IRandomAccessStream stream{ co_await streamRef.OpenReadAsync() };

		// Create the decoder from the stream
		BitmapDecoder decoder = co_await BitmapDecoder::CreateAsync(stream);

		// Get the SoftwareBitmap representation of the file
		SoftwareBitmap softwareBitmap = co_await decoder.GetSoftwareBitmapAsync();


		//convert to Bgra8 BitmapAlphaMode.Straight
		if (softwareBitmap.BitmapPixelFormat() != BitmapPixelFormat::Bgra8 ||
			softwareBitmap.BitmapAlphaMode() == BitmapAlphaMode::Straight)
		{
			softwareBitmap = SoftwareBitmap::Convert(softwareBitmap, BitmapPixelFormat::Bgra8, BitmapAlphaMode::Premultiplied);
		}

		co_return softwareBitmap;
	}
}
