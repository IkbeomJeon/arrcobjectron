#pragma once
#include "pch.h"
#include "FileHelper.h"
#include "BitmapLoader.h"
//#include "TensorConvertor.h"



#pragma region NAME_SPACE

using namespace winrt;
using namespace winrt::Windows::Foundation;
using namespace winrt::Windows::Foundation::Collections;

#ifdef USE_WINML_NUGET
using namespace winrt::Microsoft::AI::MachineLearning;
#else
using namespace Windows::AI::MachineLearning;
#endif

using namespace winrt::Windows::Media;
using namespace winrt::Windows::Graphics::Imaging;
using namespace winrt::Windows::Storage;
//using namespace winrt::Windows::Storage;
using namespace std;
using namespace BitmapLoader;
//using namespace ARRCObjectronCore;
using namespace cv;
#pragma endregion


VideoFrame LoadImageFile(hstring filePath, ColorManagementMode colorManagementMode);
std::wstring s2ws(const std::string& str);


LearningModel LoadModel(char* model_path)
{
    // Get model path
    wstring modelPath = s2ws(model_path); //L"e:/models/object_detection_3d_sneakers.onnx";

    // load the model
    //printf("Loading modelfile '%ws' on the '%s' device\n", modelPath.c_str(), deviceName.c_str());
    DWORD ticks = GetTickCount64();
    auto model = LearningModel::LoadFromFilePath(modelPath);
    ticks = GetTickCount64() - ticks;
    printf("model file loaded in %d ticks\n", ticks);

    return model;
}


ColorManagementMode GetColorManagementMode(const LearningModel& model)
{
    // Get model color space gamma
    hstring gammaSpace = L"";
    try
    {
        gammaSpace = model.Metadata().Lookup(L"Image.ColorSpaceGamma");
    }
    catch (...)
    {
        printf("    Model does not have color space gamma information. Will color manage to sRGB by default...\n");
    }
    if (gammaSpace == L"" || _wcsicmp(gammaSpace.c_str(), L"SRGB") == 0)
    {
        return ColorManagementMode::ColorManageToSRgb;
    }
    // Due diligence should be done to make sure that the input image is within the model's colorspace. There are multiple non-sRGB color spaces.
    printf("    Model metadata indicates that color gamma space is : %ws. Will not manage color space to sRGB...\n", gammaSpace.c_str());
    return ColorManagementMode::DoNotColorManage;
}
VideoFrame LoadImageFile(char* image_path, ColorManagementMode colorManagementMode)
{
    auto imageFrame = LoadImageFile(s2ws(image_path).c_str(), colorManagementMode);
    return imageFrame;
}

VideoFrame LoadImageFile(hstring filePath, ColorManagementMode colorManagementMode)
{
    // load the image
    printf("Loading the image...\n");
    BitmapDecoder decoder = NULL;
    try
    {
        // open the file
        StorageFile file = StorageFile::GetFileFromPathAsync(filePath).get();
        // get a stream on it
        auto stream = file.OpenAsync(FileAccessMode::Read).get();
        // Create the decoder from the stream
        decoder = BitmapDecoder::CreateAsync(stream).get();

    }
    catch (...)
    {
        printf("    Failed to load the image file, make sure you are using fully qualified paths\r\n");
        exit(EXIT_FAILURE);
    }
    SoftwareBitmap softwareBitmap = NULL;
    try
    {
        softwareBitmap = decoder.GetSoftwareBitmapAsync(
            decoder.BitmapPixelFormat(),
            decoder.BitmapAlphaMode(),
            BitmapTransform(),
            ExifOrientationMode::RespectExifOrientation,
            colorManagementMode
        ).get();
    }
    catch (hresult_error hr)
    {
        printf("    Failed to create SoftwareBitmap! Please make sure that input image is within the model's colorspace.\n");
        printf("    %ws\n", hr.message().c_str());
        exit(hr.code());
    }
    VideoFrame inputImage = NULL;
    try
    {
        // load a videoframe from it
        inputImage = VideoFrame::CreateWithSoftwareBitmap(softwareBitmap);

    }
    catch (hresult_error hr)
    {
        printf("Failed to create videoframe from software bitmap.");
        printf("    %ws\n", hr.message().c_str());
        exit(hr.code());
    }
    // all done
    return inputImage;
}

std::wstring s2ws(const std::string& str)
{
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo(size_needed, 0);
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}



vector<TensorFloat> Evaluate(LearningModel model, VideoFrame inputImage, string deviceName)
{
    // bind the intput image
    printf("Binding...\n");

    vector<TensorFloat> resultTensors;

    //TensorFloat inputTensor = nullptr;
    if ("GPU" == deviceName)
    {
        LearningModelSession session(model, LearningModelDevice(LearningModelDeviceKind::DirectX));
        LearningModelBinding binding(session);
        hstring inputName = model.InputFeatures().First().Current().Name();
        //binding.Bind(inputName,TensorizationHelper::SoftwareBitmapToDX12Tensor(inputImage.SoftwareBitmap()));
        binding.Bind(inputName, ImageFeatureValue::CreateFromVideoFrame(inputImage));


        // temp: bind the output (we don't support unbound outputs yet)
        vector<int64_t> shape({ 1, 1, 40, 30 });
        binding.Bind(L"Identity", TensorFloat::Create(shape));

        // now run the model
        printf("Running the model...\n");
        DWORD ticks = GetTickCount64();
        auto results = session.Evaluate(binding, L"RunId");
        ticks = GetTickCount64() - ticks;
        printf("model run took %d ticks\n", ticks);

        // get the output
        auto heatmap = results.Outputs().Lookup(L"Identity").as<TensorFloat>();
        auto offset = results.Outputs().Lookup(L"Identity_1").as<TensorFloat>();
        auto coordi = results.Outputs().Lookup(L"Identity_2").as<TensorFloat>();

        resultTensors.push_back(heatmap);
        resultTensors.push_back(offset);
        resultTensors.push_back(coordi);
    }
    else
    {
        LearningModelSession session(model, LearningModelDevice(LearningModelDeviceKind::Default));
        LearningModelBinding binding(session);
        hstring inputName = model.InputFeatures().First().Current().Name();
        //binding.Bind(inputName, TensorizationHelper::SoftwareBitmapToSoftwareTensor2(inputImage.SoftwareBitmap()));
        binding.Bind(inputName, ImageFeatureValue::CreateFromVideoFrame(inputImage));

        // temp: bind the output (we don't support unbound outputs yet)
        vector<int64_t> shape({ 1, 1, 40, 30 });
        binding.Bind(L"Identity", TensorFloat::Create(shape));

        // now run the model
        printf("Running the model...\n");
        DWORD ticks = GetTickCount64();
        auto results = session.Evaluate(binding, L"RunId");
        ticks = GetTickCount64() - ticks;
        printf("model run took %d ticks\n", ticks);

        // get the output
        auto heatmap = results.Outputs().Lookup(L"Identity").as<TensorFloat>();
        auto offset = results.Outputs().Lookup(L"Identity_1").as<TensorFloat>();
        auto coordi = results.Outputs().Lookup(L"Identity_2").as<TensorFloat>();

        resultTensors.push_back(heatmap);
        resultTensors.push_back(offset);
        resultTensors.push_back(coordi);

    }

    // bind the intput image

    

    return resultTensors;
}