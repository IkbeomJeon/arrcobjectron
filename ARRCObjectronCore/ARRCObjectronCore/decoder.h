#pragma once
#include <memory>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include "Eigen/Dense"
#include "annotation_data.h"
#include "belief_decoder_config.h"
#include <opencv2/imgcodecs.hpp>
#include "../Utils/CVUtils.h"


namespace ARRCObjectron
{
    class Decoder
    {
    public:
        static const int kNumOffsetmaps = 16;

        explicit Decoder(const BeliefDecoderConfig& config);
        // Decodes bounding boxes from predicted heatmap and offset maps.
        // Input:
        //   heatmap: a single channel cv::Mat representing center point heatmap
        //   offsetmap: a 16 channel cv::Mat representing the 16 offset maps
        //              (2 for each of the 8 vertices)
        // Output:
        //   Outputs 3D bounding boxes 2D vertices, represented by 'point_2d' field
        //   in each 'keypoints' field of object annotations.
        std::shared_ptr<FrameAnnotation> DecodeBoundingBoxKeypoints(
            const cv::Mat& heatmap, const cv::Mat& offsetmap) const;

        // Lifts the estimated 2D projections of bounding box vertices to 3D.
        // This function uses the EPnP approach described in this paper:
        // https://icwww.epfl.ch/~lepetit/papers/lepetit_ijcv08.pdf .
        // Input:
        //   projection_matrix: the projection matrix from 3D coordinate
        //     to screen coordinate.
        //     The 2D screen coordinate is defined as: u is along the long
        //     edge of the device, pointing down; v is along the short edge
        //     of the device, pointing right.
        //   portrait: a boolen variable indicating whether our images are
        //     obtained in portrait orientation or not.
        //   estimated_box: annotation with point_2d field populated with
        //     2d vertices.
        // Output:
        //   estimated_box: annotation with point_3d field populated with
        //     3d vertices.
        int Lift2DTo3D(
            const Eigen::Matrix<float, 4, 4, Eigen::RowMajor>& projection_matrix,
            bool portrait, std::shared_ptr<FrameAnnotation> estimated_box) const;

        int Lift2DTo3D(const Eigen::Matrix4f& projection_matrix, const Eigen::Matrix<float, 8, 4, Eigen::RowMajor>& epnp_alpha
            , const std::vector<Eigen::Vector2f>& keypoints_2d, std::vector<Eigen::Vector3f>& keypoints_3d);

    //protected:
        struct BeliefBox {
            float belief;
            std::vector<std::pair<float, float>> box_2d;
        };

        std::vector<cv::Point> ExtractCenterKeypoints(
            const cv::Mat& center_heatmap) const;

        // Decodes 2D keypoints at the peak point.
        void DecodeByPeak(const cv::Mat& offsetmap, int center_x, int center_y,
            float offset_scale_x, float offset_scale_y,
            BeliefBox* box) const;

        // Decodes 2D keypoints by voting around the peak.
        void DecodeByVoting(const cv::Mat& heatmap, const cv::Mat& offsetmap,
            int center_x, int center_y, float offset_scale_x,
            float offset_scale_y, BeliefBox* box) const;

        // Returns true if it is a new box. Otherwise, it may replace an existing box
        // if the new box's belief is higher.   
        bool IsNewBox(std::vector<BeliefBox>* boxes, BeliefBox* box) const;

        // Returns true if the two boxes are identical.
        bool IsIdentical(const BeliefBox& box_1,
            const BeliefBox& box_2) const;

        BeliefDecoderConfig config_;

        // Following equation (1) in this paper
        // https://icwww.epfl.ch/~lepetit/papers/lepetit_ijcv08.pdf,
        // this variable denotes the coefficients for the 4 control points
        // for each of the 8 3D box vertices.
        Eigen::Matrix<float, 8, 4, Eigen::RowMajor> epnp_alpha_;

    };
  
}
