#pragma once
#include "belief_decoder_config.h"
namespace ARRCObjectron
{
	class TensorsToObjectsCalculatorOptions {

	public:
		// The number of output classes predicted by the detection model.
		int num_classes = 1;

		// The number of predicted keypoints.
		int num_keypoints = 9;
		// The dimension of each keypoint, e.g. number of values predicted for each
		// keypoint.
		int num_values_per_keypoint = 2;

		BeliefDecoderConfig decoder_config;

		TensorsToObjectsCalculatorOptions() = default;

		TensorsToObjectsCalculatorOptions(int num_classes, int num_keypoints, int num_values_per_keypoint, const BeliefDecoderConfig& decoder_config)
			: num_classes(num_classes), num_keypoints(num_keypoints), num_values_per_keypoint(num_values_per_keypoint), decoder_config(decoder_config)
		{
		}

		TensorsToObjectsCalculatorOptions(const BeliefDecoderConfig& decoder_config) : decoder_config(decoder_config) {}

		
	};
}