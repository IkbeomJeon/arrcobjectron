#pragma once
// Copyright 2020 The MediaPipe Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "object.h"
#include <vector>
#include <memory>

namespace ARRCObjectron
{
	// Projection of a 3D point on an image, and its metric depth.
	class NormalizedPoint2D {
	public:
		// x-y position of the 2d keypoint in the image coordinate system.
		// u,v \in [0, 1], where top left corner is (0, 0) and the bottom-right corner
		// is (1, 1).
		float x;
		float y;

		// The depth of the point in the camera coordinate system (in meters).
		float depth;


	};

	// The 3D point in the camera coordinate system, the scales are in meters.
	class Point3D {
	public:
		float x;
		float y;
		float z;
	};

	class AnnotatedKeyPoint {
	private:
		const int id;
		//std::shared_ptr<Point3D> _point_3d;
		//std::shared_ptr<NormalizedPoint2D> _point_2d;

		std::shared_ptr<Point3D> _point_3d;
		std::shared_ptr<NormalizedPoint2D> _point_2d;
		static int genID()
		{
			static int newID = 0;
			return newID++;
		}
	public:
		AnnotatedKeyPoint() : id(genID())
		{
			_point_3d = std::make_shared< Point3D>();
			_point_2d = std::make_shared< NormalizedPoint2D>();
		}
		//AnnotatedKeyPoint(const AnnotatedKeyPoint&) = default;
		std::shared_ptr<NormalizedPoint2D> mutable_point_2d()
		{
			//auto out = std::make_shared<NormalizedPoint2D>();
			return _point_2d;
		}
		const NormalizedPoint2D& point_2d()
		{
			return *_point_2d;
		}
		std::shared_ptr<Point3D> mutable_point_3d()
		{
			//auto out = std::make_shared<Point3D>();
			return _point_3d;
		}
		const Point3D& point_3d()
		{
			return *_point_3d;
		}
	};

	class ObjectAnnotation {
		// Reference to the object identifier in ObjectInstance.
		const int _object_id;

		// For each objects, list all the annotated keypoints here.
		// E.g. for bounding-boxes, we have 8 keypoints, hands = 21 keypoints, etc.
		// These normalized points are the projection of the Object's 3D keypoint
		// on the current frame's camera poses.
		std::vector<std::shared_ptr<AnnotatedKeyPoint>> _keypoints;

		// Visibiity of this annotation in a frame.
		float visibility;
		static int genID()
		{
			static int newID = 0;
			return newID++;
		}
	public:
		ObjectAnnotation() : _object_id(genID()) {}

		int object_id()
		{
			return _object_id;
		}
		//ObjectAnnotation(const ObjectAnnotation&) = default;
		std::shared_ptr<AnnotatedKeyPoint> add_keypoints()
		{
			auto newKeyPoint = std::make_shared<AnnotatedKeyPoint>();
			_keypoints.push_back(newKeyPoint);
			return newKeyPoint;
		}
		std::vector<std::shared_ptr<AnnotatedKeyPoint>> mutable_keypoints()
		{
			return _keypoints;
			//auto out = std::make_shared<std::vector<AnnotatedKeyPoint>>();
			//return std::make_shared<std::vector<std::shared_ptr<AnnotatedKeyPoint>>>(_keypoints);
		}

		std::shared_ptr<AnnotatedKeyPoint> mutable_keypoints(int idx)
		{
			//return _keypoints.begin() + idx;
			//auto copied = std::make_shared<AnnotatedKeyPoint>();
			return _keypoints[idx];
		}

		const std::vector<std::shared_ptr<AnnotatedKeyPoint>> keypoints() const
		{
			return _keypoints;
		}

		const AnnotatedKeyPoint& keypoints(int idx) const
		{
			return *_keypoints[idx];
		}
	};

	class FrameAnnotation {
		// Unique frame id, corresponds to images.
		const int frame_id;

		// List of the annotated objects in this frame. Depending on how many object
		// are observable in this frame, we might have non or as much as
		// sequence.objects_size() annotations.
		std::vector<std::shared_ptr<ObjectAnnotation>> _annotations;

		// Information about the camera transformation (in the world coordinate) and
		// imaging characteristics for a captured video frame.
		//ARCamera camera = 3;

		// The timestamp for the frame.
		double timestamp;

		// Plane center and normal in camera frame.
		std::vector<float> plane_center;
		std::vector<float> plane_normal;
		static int genID()
		{
			static int newID = 0;
			return newID++;
		}
	public:
		FrameAnnotation() : frame_id(genID()) {}
		//FrameAnnotation(const FrameAnnotation&) = default;

		std::shared_ptr<ObjectAnnotation> add_annotations()
		{
			auto newAnnotation = std::make_shared<ObjectAnnotation>();
			_annotations.push_back(std::shared_ptr<ObjectAnnotation>(newAnnotation));
			return newAnnotation;
		}
		std::vector<std::shared_ptr<ObjectAnnotation>> mutable_annotations()
		{
			//auto out = std::make_shared <std::vector<ObjectAnnotation>>();

			return _annotations;
		}

		const std::vector<std::shared_ptr<ObjectAnnotation>>& annotations() const
		{
			return _annotations;
		}

	};

	// The sequence protocol contains the annotation data for the entire video clip.
	class Sequence {
	public:
		Sequence() = default;
		//Sequence(const Sequence&) = default;
		// List of all the annotated 3D objects in this sequence in the world
		// Coordinate system. Given the camera poses of each frame (also in the
		// world-coordinate) these objects bounding boxes can be projected to each
		// frame to get the per-frame annotation (i.e. image_annotation below).
		std::vector<Object*> objects;

		// List of annotated data per each frame in sequence + frame information.
		std::vector<FrameAnnotation*> frame_annotations;
	};

}
