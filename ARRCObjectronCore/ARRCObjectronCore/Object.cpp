#include "object.h"
namespace ARRCObjectron
{
	std::string Object::category() const
	{
		return _category;
	}

	std::vector<float> Object::rotation() const
	{
		return _rotation;
	}

	std::vector<float> Object::translation() const
	{
		return _translation;
	}

	std::vector<float> Object::scale() const
	{
		return _scale;
	}

	void Object::set_category(const std::string category)
	{
		_category = category.c_str();
	}

	void Object::set_type(const Type type)
	{
		_type = type;
	}

	void Object::add_translation(float value)
	{
		_translation.push_back(value);
	}

	void Object::add_rotation(float value)
	{
		_rotation.push_back(value);
	}

	void Object::add_scale(float value)
	{
		_scale.push_back(value);
	}

	std::shared_ptr<KeyPoint> Object::add_keypoints()
	{
		std::shared_ptr newKeyPoint = std::make_shared<KeyPoint>();
		_keypoints.push_back(newKeyPoint);

		return newKeyPoint;
	}

}
