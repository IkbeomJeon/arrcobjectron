#pragma once
#include <memory>
#include <iostream>
#include "Eigen/Dense"
#include "Eigen/src/Core/util/Constants.h"
#include "Eigen/src/Geometry/Quaternion.h"
#include "annotation_data.h"

#include "box.h"
#include "ModelMatrix.h"
//#include "mediapipe/util/color.pb.h"

namespace ARRCObjectron {

    using Matrix4fRM = Eigen::Matrix<float, 4, 4, Eigen::RowMajor>;

    // Converts the box prediction from Objectron Model to the Model matrices
    // to be rendered.
    //
    // Input:
    //  ANNOTATIONS - Frame annotations with lifted 3D points, the points are in
    //     Objectron coordinate system.
    // Output:
    //  MODEL_MATRICES - Result ModelMatrices, in OpenGL coordinate system.
    //
    // Usage example:
    // node {
    //  calculator: "AnnotationsToModelMatricesCalculator"
    //  input_stream: "ANNOTATIONS:objects"
    //  output_stream: "MODEL_MATRICES:model_matrices"
    //}
    class AnnotationsToModelMatricesCalculatorOptions {

    };
    class AnnotationsToModelMatricesCalculator
    {

    public:
        AnnotationsToModelMatricesCalculator(Eigen::Vector3f scale)
        {
            model_scale_ = scale;
            //model_transformation_.setIdentity();

            model_transformation_ <<
                0, 0, -1, 0,
                0, 1, 0, 0,
                1, 0, 0, 0,
                0, 0, 0, 1;


            //[0, 0, -1, 0] , [0, 1, 0, 0], [1, 0, 0, 0], [0, 0, 0, 1]
        }
        ~AnnotationsToModelMatricesCalculator() {}
        AnnotationsToModelMatricesCalculator(
            const AnnotationsToModelMatricesCalculator&) = delete;
        AnnotationsToModelMatricesCalculator& operator=(
            const AnnotationsToModelMatricesCalculator&) = delete;

        TimedModelMatrixProtoList* Process(const std::shared_ptr<FrameAnnotation> annotations) {
            auto model_matrices = std::make_unique<TimedModelMatrixProtoList>();

            int result = GetModelMatricesForAnnotations(annotations, model_matrices.get());
            assert(result == 0 && "Error in GetModelMatricesForBoxes");
        
            return model_matrices.release();
        }

    private:
        int GetModelMatricesForAnnotations(const std::shared_ptr<FrameAnnotation> annotations, TimedModelMatrixProtoList* model_matrix_list)
        {
            assert(model_matrix_list != nullptr && "model_matrix_list is nullptr");
            //model_matrix_list->clear_model_matrix();

            Box box("category");
            for (const auto& object : annotations->annotations())
            {
                std::shared_ptr<TimedModelMatrixProto> model_matrix = model_matrix_list->add_model_matrix();
                model_matrix->set_id(object->object_id());

                // Fit a box to the original vertices to estimate the scale of the box
                std::vector<Eigen::Vector3f> vertices;
                for (const auto& keypoint : object->keypoints()) {
                    const auto& point = keypoint->point_3d();
                    Eigen::Vector3f p(point.x, point.y, point.z);
                    vertices.emplace_back(p);
                }
                box.Fit(vertices);

                // Re-scale the box if necessary
                Eigen::Vector3f estimated_scale = box.GetScale();
                vertices.clear();
                for (const auto& keypoint : object->keypoints()) {
                    const auto& point = keypoint->point_3d();
                    Eigen::Vector3f p(point.x, point.y, point.z);
                    vertices.emplace_back(p);
                }

                Matrix4fRM object_transformation = box.GetTransformation();
                Matrix4fRM model_view;
                Matrix4fRM pursuit_model;
                // The reference view is
                //
                // ref <<  0.,  0.,  1.,  0.,
                //        -1.,  0., 0.,  0.,
                //         0.,  -1.,  0.,  0.,
                //         0.,  0.,  0.,  1.;
                // We have pursuit_model * model = model_view, to get pursuit_model:
                // pursuit_model = model_view * model^-1
                // clang-format off
                pursuit_model << 0.0, 1.0, 0.0, 0.0,
                    1.0, 0.0, 0.0, 0.0,
                    0.0, 0.0, 1.0, 0.0,
                    0.0, 0.0, 0.0, 1.0;
                // clang-format on

                // Re-scale the CAD model to the scale of the estimated bounding box.
                const Eigen::Vector3f scale = model_scale_.cwiseProduct(estimated_scale);
                const Matrix4fRM model =
                    model_transformation_.array().colwise() * scale.homogeneous().array();

                // Finally compute the model_view matrix.
                model_view = pursuit_model * object_transformation * model;
                //model_view =  object_transformation * model;
                model_matrix->add_matrix_entries(model_view);
                /*for (int i = 0; i < model_view.rows(); ++i) {
                    for (int j = 0; j < model_view.cols(); ++j) {
                        model_matrix->add_matrix_entries(model_view(i, j));
                    }
                }*/
                return 0;
            }
            
        }

        AnnotationsToModelMatricesCalculatorOptions options_;
        Eigen::Vector3f model_scale_;
        Matrix4fRM model_transformation_;
    };
    
 

}  // namespace mediapipe
