#include "Tensors_to_objects_calculator.h"

namespace ARRCObjectron
{
    TensorsToObjectsCalculator::TensorsToObjectsCalculator(const TensorsToObjectsCalculatorOptions& options)
    {
        options_ = options;
        num_classes_ = options_.num_classes;
        num_keypoints_ = options_.num_keypoints;

        // Currently only support 2D when num_values_per_keypoint equals to 2.
        //CHECK_EQ(options_.num_values_per_keypoint(), 2);

        // clang-format off
        projection_matrix_ <<
            1.5731, 0, 0, 0,
            0, 2.0975, 0, 0,
            0, 0, -1.0002, -0.2,
            0, 0, -1, 0;

        // clang-format on

  //      projection_matrix_ <<
  //          572.4114, 0, 325.2611, 0,
  //          0, 573.57043, 242.04899, 0,
  //          0, 0, 16*30.0000, 0,
  //          0, 0, 1, 0;
  ///*      projection_matrix_(0) /= (16 * 30);
  //      projection_matrix_(1) /= (16 * 30);*/

        decoder_ = std::make_unique<Decoder>(
            BeliefDecoderConfig(options_.decoder_config));
    }

    std::shared_ptr<FrameAnnotation> TensorsToObjectsCalculator::Process(const cv::Mat& prediction_heatmap, const cv::Mat& offsetmap)
    {
        std::shared_ptr<FrameAnnotation> output_objects;

        output_objects =
            decoder_->DecodeBoundingBoxKeypoints(prediction_heatmap, offsetmap);

        auto status = decoder_->Lift2DTo3D(projection_matrix_, /*portrait*/ false,
            output_objects);

        /*if (!status.ok()) {
        LOG(ERROR) << status;
        return status;
        }*/
        Project3DTo2D(/*portrait*/ false, output_objects);
        /*AssignObjectIdAndTimestamp(cc->InputTimestamp().Microseconds(),
        output_objects);*/

        return output_objects;
    }

    void TensorsToObjectsCalculator::Project3DTo2D(
        bool portrait, std::shared_ptr<FrameAnnotation> annotation) const {
        for (auto ann : annotation->mutable_annotations()) {
            for (auto key_point : ann->mutable_keypoints()) {
                Eigen::Vector4f point3d;
                //point3d << key_point->point_3d().x*38, key_point->point_3d().y*37,
                //    key_point->point_3d().z*45, 1.0f;
                //Eigen::Vector4f point3d_projection = projection_matrix_2 * point3d;

                point3d << key_point->point_3d().x, key_point->point_3d().y,
                    key_point->point_3d().z, 1.0f;
                Eigen::Vector4f point3d_projection = projection_matrix_ * point3d;

                float u, v;
                const float inv_w = 1.0f / point3d_projection(3);
                if (portrait) {
                    u = (point3d_projection(1) * inv_w + 1.0f) * 0.5f;
                    v = (point3d_projection(0) * inv_w + 1.0f) * 0.5f;
                }
                else {
                    u = (point3d_projection(0) * inv_w + 1.0f) * 0.5f;
                    v = (1.0f - point3d_projection(1) * inv_w) * 0.5f;
                }
               /* u = 640-point3d_projection(0) * inv_w;
                v = point3d_projection(1) * inv_w;*/
     
                key_point->mutable_point_2d()->x = u;
                key_point->mutable_point_2d()->y = v;
            }
        }
    }

    //void TensorsToObjectsCalculator::AssignObjectIdAndTimestamp(
    //    int64 timestamp_us, FrameAnnotation* annotation) {
    //    for (auto& ann : *annotation->mutable_annotations()) {
    //        ann.set_object_id(GetNextObjectId());
    //    }
    //    annotation->set_timestamp(timestamp_us);
    //}

}