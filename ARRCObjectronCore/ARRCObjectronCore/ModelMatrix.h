#pragma once
#include <vector>
#include <memory>
namespace ARRCObjectron {

	using Matrix4fRM = Eigen::Matrix<float, 4, 4, Eigen::RowMajor>;

	class TimedModelMatrixProto
	{
		// 4x4 model matrix stored in ROW major order.
		//std::vector<float> matrix_entries;

		// Unique per object id
		Matrix4fRM _model_matrix;
		int id = -1;

	public:
		void set_id(int id)
		{
			this->id = id;
		}

		void add_matrix_entries(Matrix4fRM model_matrix)
		{
			_model_matrix = model_matrix;
		}
		Matrix4fRM model_matrix()
		{
			return _model_matrix;
		}
	};
	
	class TimedModelMatrixProtoList {
		std::vector<std::shared_ptr<TimedModelMatrixProto>> _model_matrix_proto;
		//std::vector<std::shared_ptr<Matrix4fRM>> model_matrix;

	public:
		std::shared_ptr<TimedModelMatrixProto> add_model_matrix()
		{
			auto newMatrix = std::make_shared<TimedModelMatrixProto>();
				
			_model_matrix_proto.push_back(newMatrix);
			return newMatrix;
		}
		const std::vector<std::shared_ptr<TimedModelMatrixProto>> model_matrix_proto() const
		{
			return _model_matrix_proto;
		}
		
	};

	// For convenience, when the desired information or transformation can be
	// encoded into vectors (e.g. when the matrix represents a scale or Euler-angle-
	// based rotation operation.)
	class TimedVectorProto
	{
		// The vector values themselves.
		std::vector<float> vector_entries;

		// Unique per object id
		int id = -1;
	};

	class TimedVectorProtoList {
	public:

		std::vector<TimedVectorProto> vector_list;
	};
}