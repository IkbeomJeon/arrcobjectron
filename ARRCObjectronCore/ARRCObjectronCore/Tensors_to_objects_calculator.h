#pragma once
#include <memory>
#include "decoder.h"
#include "Eigen/Dense"
#include "opencv2/core.hpp"
#include "TensorsToObjectsCalculatorOptions.h"

namespace ARRCObjectron
{
	class TensorsToObjectsCalculator
	{
	public:
		TensorsToObjectsCalculator() = default;
		TensorsToObjectsCalculator(const TensorsToObjectsCalculatorOptions& options);
		std::shared_ptr<FrameAnnotation> Process(const cv::Mat& prediction_heatmap, const cv::Mat& offsetmap);

	private:
		// Takes point_3d in FrameAnnotation, projects to 2D, and overwrite the
		// point_2d field with the projection.
		void Project3DTo2D(bool portrait, std::shared_ptr<FrameAnnotation> annotation) const;
		// Increment and assign object ID for each detected object.
		// In a single MediaPipe session, the IDs are unique.
		// Also assign timestamp for the FrameAnnotation to be the input packet
		// timestamp.
		//void AssignObjectIdAndTimestamp(int64 timestamp_us,
		//	FrameAnnotation* annotation);

		int num_classes_ = 0;
		int num_keypoints_ = 0;

		TensorsToObjectsCalculatorOptions options_;
		std::unique_ptr<Decoder> decoder_;
		Eigen::Matrix<float, 4, 4, Eigen::RowMajor> projection_matrix_, projection_matrix_2;
	};

	
}

