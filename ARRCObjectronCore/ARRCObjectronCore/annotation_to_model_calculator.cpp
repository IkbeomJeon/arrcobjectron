
::mediapipe::Status
AnnotationsToModelMatricesCalculator::GetModelMatricesForAnnotations(
    const FrameAnnotation& annotations,
    TimedModelMatrixProtoList* model_matrix_list) {
    if (model_matrix_list == nullptr) {
        return ::mediapipe::InvalidArgumentError("model_matrix_list is nullptr");
    }
    model_matrix_list->clear_model_matrix();

    Box box("category");
    for (const auto& object : annotations.annotations()) {
        TimedModelMatrixProto* model_matrix = model_matrix_list->add_model_matrix();
        model_matrix->set_id(object.object_id());

        // Fit a box to the original vertices to estimate the scale of the box
        std::vector<Eigen::Vector3f> vertices;
        for (const auto& keypoint : object.keypoints()) {
            const auto& point = keypoint.point_3d();
            Eigen::Vector3f p(point.x(), point.y(), point.z());
            vertices.emplace_back(p);
        }
        box.Fit(vertices);

        // Re-scale the box if necessary
        Eigen::Vector3f estimated_scale = box.GetScale();
        vertices.clear();
        for (const auto& keypoint : object.keypoints()) {
            const auto& point = keypoint.point_3d();
            Eigen::Vector3f p(point.x(), point.y(), point.z());
            vertices.emplace_back(p);
        }
        box.Fit(vertices);

        Matrix4fRM object_transformation = box.GetTransformation();
        Matrix4fRM model_view;
        Matrix4fRM pursuit_model;
        // The reference view is
        //
        // ref <<  0.,  0.,  1.,  0.,
        //        -1.,  0., 0.,  0.,
        //         0.,  -1.,  0.,  0.,
        //         0.,  0.,  0.,  1.;
        // We have pursuit_model * model = model_view, to get pursuit_model:
        // pursuit_model = model_view * model^-1
        // clang-format off
        pursuit_model << 0.0, 1.0, 0.0, 0.0,
            1.0, 0.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0;
        // clang-format on

        // Re-scale the CAD model to the scale of the estimated bounding box.
        const Eigen::Vector3f scale = model_scale_.cwiseProduct(estimated_scale);
        const Matrix4fRM model =
            model_transformation_.array().colwise() * scale.homogeneous().array();

        // Finally compute the model_view matrix.
        model_view = pursuit_model * object_transformation * model;

        for (int i = 0; i < model_view.rows(); ++i) {
            for (int j = 0; j < model_view.cols(); ++j) {
                model_matrix->add_matrix_entries(model_view(i, j));
            }
        }
    }
    return ::mediapipe::OkStatus();
}
