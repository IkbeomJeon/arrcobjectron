// Copyright 2020 The MediaPipe Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once;

#include <vector>
#include <string>
#include <memory>

namespace ARRCObjectron
{
    class KeyPoint {
        // The position of the keypoint in the local coordinate system of the rigid
        // object.
        float _x;
        float _y;
        float _z;

        // Sphere around the keypoint, indiciating annotator's confidence of the
        // position in meters.
        float confidence_radius;

        // The name of the keypoint (e.g. legs, head, etc.).
        // Does not have to be unique.
        const char* name;

        // Indicates whether the keypoint is hidden or not.
        bool hidden;
    public:
        void set_x(float x) {
            _x = x;
        }
        void set_y(float y) {
            _y = y;
        }
        void set_z(float z) {
            _z = z;
        }
        void set_confidence_radius(float val)
        {
            confidence_radius = val;
        }
     

    };

    class Object {
       
   
    public:
        enum class Method {
            UNKNOWN_METHOD = 0,
            ANNOTATION,       // Created by data annotation.
            AUGMENTATION,  // Created by data augmentation.
        };

        enum class Type {
            UNDEFINED_TYPE = 0,
            BOUNDING_BOX = 1,
            SKELETON = 2,
        };

        Object() = default;
        Object(const Object&) = default;

        std::string category() const;
        std::vector<float> rotation() const;
        std::vector<float> translation() const;
        std::vector<float> scale() const;
        void set_category(const std::string category);
        void set_type(const Type type);
        void add_translation(const float value);
        void add_rotation(const float value);
        void add_scale(const float value);
        std::shared_ptr<KeyPoint> add_keypoints();
    private:
        // Unique object id through a sequence. There might be multiple objects of
       // the same label in this sequence.
        int _id;

        Type _type;

        Method _method;

        // Describes what category an object is. E.g. object class, attribute,
       // instance or person identity. This provides additional context for the
       // object type.
        const char* _category;

        // 3x3 row-major rotation matrix describing the orientation of the rigid
        // object's frame of reference in the world-coordinate system.
        std::vector<float> _rotation;

        // 3x1 vector describing the translation of the rigid object's frame of
        // reference in the world-coordinate system in meters.
        std::vector<float> _translation;

        // 3x1 vector describing the scale of the rigid object's frame of reference in
        // the world-coordinate system in meters.
        std::vector<float> _scale;

        // List of all the key points associated with this object in the object
        // coordinate system.
        // The first keypoint is always the object's frame of reference,
        // e.g. the centroid of the box.
        // E.g. bounding box with its center as frame of reference, the 9 keypoints :
        // {0., 0., 0.},
        // {-.5, -.5, -.5}, {-.5, -.5, +.5}, {-.5, +.5, -.5}, {-.5, +.5, +.5},
        // {+.5, -.5, -.5}, {+.5, -.5, +.5}, {+.5, +.5, -.5}, {+.5, +.5, +.5}
        // To get the bounding box in the world-coordinate system, we first scale the
        // box then transform the scaled box.
        // For example, bounding box in the world coordinate system is
        // rotation * scale * keypoints + translation
        std::vector<std::shared_ptr<KeyPoint>> _keypoints;
        // Enum to reflect how this object is created.
    };

    // The edge connecting two keypoints together
    class Edge {
        // keypoint id of the edge's source
        int source;

        // keypoint id of the edge's sink
        int sink;
    };

    // The skeleton template for different objects (e.g. humans, chairs, hands, etc)
    // The annotation tool reads the skeleton template dictionary.
    class Skeleton {
        // The origin keypoint in the object coordinate system. (i.e. Point 0, 0, 0)
        int reference_keypoint;

        // The skeleton's category (e.g. human, chair, hand.). Should be unique in the
        // dictionary.
        const char* category;

        // Initialization value for all the keypoints in the skeleton in the object's
        // local coordinate system. Pursuit will transform these points using object's
        // transformation to get the keypoint in the world-cooridnate.
        std::vector<KeyPoint> keypoints;

        // List of edges connecting keypoints
        std::vector<Edge> edges;
    };

    // The list of all the modeled skeletons in our library. These models can be
    // objects (chairs, desks, etc), humans (full pose, hands, faces, etc), or box.
    // We can have multiple skeletons in the same file.
    class Skeletons {
        std::vector<Skeleton> object;
    };

}
