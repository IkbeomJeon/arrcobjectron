#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "Box.h"

static void DrawFace(cv::Mat& target_image, std::vector<cv::Point> points,
    const ARRCObjectron::Face face, cv::Scalar color, int thickness)
{
    cv::Point start;
    cv::Point end;

    start = points[face[0]];
    end = points[face[2]];

    cv::line(target_image, start, end, color, thickness);

    start = points[face[1]];
    end = points[face[3]];

    cv::line(target_image, start, end, color, thickness);

}
static void DrawBoundingBox(cv::Mat& target_image,
    const std::vector<cv::Point>& points, cv::Scalar color, int thickniss)
{
    auto edges = ARRCObjectron::Box("dummy").GetEdges();
    auto faces = ARRCObjectron::Box("dummy").GetFaces();


    for (size_t i = 0; i < points.size(); i++)
    {
        //needs to hidden check;

        for (const auto& edge : edges)
        {
            if (edge[0] < points.size() && edge[1] < points.size())
            {
                auto start = points[edge[0]];
                auto end = points[edge[1]];

                cv::line(target_image, start, end, color, thickniss);
            }
        }
    }

    DrawFace(target_image, points, faces[ARRCObjectron::kFrontFaceId], cv::Scalar(0, 255, 255), thickniss);
    DrawFace(target_image, points, faces[ARRCObjectron::kTopFaceId], cv::Scalar(0, 0, 255), thickniss);


    std::vector<bool> hidden(points.size(), false);

}
//static cv::Mat DrawBoundingBox(const cv::Mat src_image, const std::vector<cv::Point> keyPoints, cv::Scalar color, int thickness = 1)
static void DrawAnnotations(cv::Mat& target_image, ARRCObjectron::FrameAnnotation frame_annotation)
{
    auto& annotations = frame_annotation.annotations();

    int num_objects = annotations.size();
    for (const auto& ann : annotations)
    {
        const auto pKeypoints = ann.get()->keypoints(); //const

        std::vector<cv::Point> points_pixel;
        int width = target_image.cols;
        int height = target_image.rows;

        for_each(pKeypoints.begin(), pKeypoints.end(),
            [&points_pixel, width, height](const std::shared_ptr< ARRCObjectron::AnnotatedKeyPoint> pkp)
            {
                int x = pkp->point_2d().x * width;
                int y = pkp->point_2d().y * height;
                /*int x = pkp->point_2d().x * 1;
                int y = pkp->point_2d().y * 1;*/

                points_pixel.push_back(cv::Point(x, y));
            }
        );

        DrawBoundingBox(target_image, points_pixel, cv::Scalar(255, 0, 0), 2);
    }


}
