#pragma once
class BeliefDecoderConfig
{
public:
    float heatmap_threshold = 0.7f;
    //float heatmap_threshold = 0.3f;
    // Maximum distance in pixels between two local max heatmap values.
    float local_max_distance = 2.f;
    // Coefficient of offset_scale.
    // offset_scale = offset_scale_coef * min(rows, cols).
    // offset_scale is used to multiply the offset predictions from the network.
    float offset_scale_coef = 1.f;

    // The radius for vertex voting. Use no voting if the radius is less than or
    // euqal to 1. Example: 10.
    int voting_radius = 2;

    // The number of pixels to determine whether two points are the same.
    // Example: 5 (voting_radius / 2).
    int voting_allowance = 3;

    // The threshold of beliefs, with which the points can vote. Example: 0.2.
    float voting_threshold = 0.2f;

    BeliefDecoderConfig() 
    {
    }
    BeliefDecoderConfig(
        float heatmap_threshold, float local_max_distance, float offset_scale_coef
        , int voting_radius, int voting_allowance, float voting_threshold) :
        heatmap_threshold(heatmap_threshold), local_max_distance(local_max_distance), offset_scale_coef(offset_scale_coef),
        voting_radius(voting_radius), voting_allowance(voting_allowance), voting_threshold(voting_threshold)
    {}

};
