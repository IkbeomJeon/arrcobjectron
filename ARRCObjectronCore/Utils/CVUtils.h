#pragma once
//#include "opencv2/ml//ml.hpp"
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"

static cv::Mat ConvertFloatMatToUC3(const cv::Mat src);

static cv::Mat Crop(const cv::Mat& input, int width, int height)
{
    double required_spectRatio = width / (double)height;
    double aspectRatio = input.size().aspectRatio();
    int input_width = input.size().width;
    int input_height = input.size().height;

    cv::Mat cropped;
    if (aspectRatio > 1.0)
    {
        int crop_width = (int)(required_spectRatio * input_height);
        int offset_width = (input_width - crop_width) / 2;
        cropped = input(cv::Rect(offset_width, 0, crop_width, input_height));
    }
    else
    {
        int crop_height = (int)(1.0 / required_spectRatio * input_width);
        int offset_height = (input_height - crop_height) / 2;
        cropped = input(cv::Rect(0, offset_height, input_width, crop_height));
    }

   
    return cropped;
}

static cv::Mat ConvertToHeatMap(const cv::Mat probability)
{
    cv::Mat heatmap;
    cv::Mat uc3 = ConvertFloatMatToUC3(probability);
    applyColorMap(uc3, heatmap, cv::COLORMAP_JET);

    return heatmap;
}

static cv::Mat ConvertFloatMatToUC3(const cv::Mat src)
{
    cv::Mat uc3;
    cv::normalize(src, uc3, 0, 255, cv::NORM_MINMAX);
    uc3.convertTo(uc3, CV_8UC3);
    //cv::add(src, cv::Mat::ones(cv::Size(src.cols, src.rows), src.type()), src);
    ///*cv::FileStorage fp("debug.yml", cv::FileStorage::WRITE);
    //fp << src;
    //fp.release();*/
    //src.convertTo(src, src.type(), 255.);
    //src.convertTo(uc3, CV_8UC3);

    return uc3;
}

static cv::Mat Transpose(const cv::Mat src)
{
    cv::Mat dst;
    cv::transpose(src, dst);
    return dst;
}

static cv::Mat NormalizeUsingMeanStd(cv::Mat img, cv::Scalar mean, cv::Scalar std)
{
    cv::Mat channels[3];
    cv::split(img, channels);
    channels[0] = (channels[0] - mean[0]) / std[0]; //b
    channels[1] = (channels[1] - mean[1]) / std[1]; //g
    channels[2] = (channels[2] - mean[2]) / std[2]; //r
    cv::Mat img_norm;
    cv::merge(channels, 3, img_norm);

    return img_norm;
}

