#pragma once
#include <vector>
#include <onnxruntime_cxx_api.h>
#include <numeric>
#include <opencv2/core.hpp>
#include <memory>

static Ort::Value CreateCHWTensor_float32(const cv::Mat mat);
static Ort::Value CreateTensor(float* tensor_value, size_t tensorSize, const int64_t* shape, size_t shape_len);
static void hwc_to_chw(cv::InputArray src, cv::OutputArray& dst);
static void chw_to_hwc(cv::InputArray src, cv::OutputArray& dst);

static void hwc_to_chw(const uchar* input, size_t h, size_t w, float** output, size_t* output_count);
static void hwc_to_chw(const float* input, size_t h, size_t w, float** output, size_t* output_count);
static void hwc_to_cwh(const uchar* input, const size_t H, const size_t W, const size_t C, float** output, size_t* output_count);

static void ReleaseTensor(const Ort::Value* tensor);

/// <summary>
/// type of mat ; The type of mat is assumed to 'uchar' here.
/// layout;
/// </summary>
/// <param name="mat"></param>
/// <param name="div"></param>
/// <returns></returns>
/// 
static Ort::Value CreateCHWTensor_float32(const cv::Mat mat)
{
    //needs to refectoring to generalization.
    float* cwh_data;
    size_t output_size;
    
    std::vector<int64_t> shape = { 1, mat.channels(), mat.rows,   mat.cols}; 
    hwc_to_chw((float*)mat.data, shape[3], shape[2], &cwh_data, &output_size);
    return CreateTensor(cwh_data, output_size, shape.data(), shape.size());
}


static Ort::Value CreateTensor(float* tensor_value, const size_t tensorSize, const int64_t* shape, const size_t shape_len)
{
    // create input tensor object from data values
    auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);
    return Ort::Value::CreateTensor<float>(memory_info, tensor_value, tensorSize, shape, shape_len);
}
static cv::Mat CreateMat_from_CHWTensor_float32(const Ort::Value* tensor)
{
    int idx[8] = { 3, 2, 7, 6, 0, 1, 4, 5 };

    auto info = tensor->GetTensorTypeAndShapeInfo();
    auto shape = info.GetShape();
    auto type = info.GetElementType();

    size_t C = shape[1];
    size_t H = shape[2];
    size_t W = shape[3];

    const float* tensorData = tensor->GetTensorData<float>(); // warn ; not mutable.

    cv::Mat output_cwh(cv::Size(H, W), CV_32FC(C));
    cv::Mat output_chw(cv::Size(W, H), CV_32FC(C));

    float* matData = (float*)output_chw.data;

    //deep copy.
    for (size_t ci = 0; ci < C; ci++)
    {
        for (size_t hi = 0; hi < H; hi++)
        {
            for (size_t wi = 0; wi < W; wi++)
            {
                //
                int tensor_idx = (ci * W * H) + (hi * W) + wi;   //c h w
                size_t mat_idx = (hi * C * W) + (wi * C) + ci;   //h w c

                //int tensor_idx = (ci * W * H) + (wi * H) + hi;   //c w h
                //size_t mat_idx = (wi * C * W) + (hi * C) + ci;   //w h c
                
                matData[mat_idx] = tensorData[tensor_idx];

           /*     if(hi<23 && hi>7 && wi<30 && wi>10)
                    matData[mat_idx] = tensorData[tensor_idx];
                else
                    matData[mat_idx] = 0;*/
            }
        }
    }
    //cv::transpose(output_cwh, output_chw);
    return output_chw;
}

static void hwc_to_chw(cv::InputArray src, cv::OutputArray& dst) {
    const int src_h = src.rows();
    const int src_w = src.cols();
    const int src_c = src.channels();

    cv::Mat hw_c = src.getMat().reshape(1, src_h * src_w);

    const std::array<int, 3> dims = { src_c, src_h, src_w };
    dst.create(3, &dims[0], CV_MAKETYPE(src.depth(), 1));
    cv::Mat dst_1d = dst.getMat().reshape(1, { src_c, src_h, src_w });

    cv::transpose(hw_c, dst_1d);
}

static void chw_to_hwc(cv::InputArray src, cv::OutputArray& dst) {
    const auto& src_size = src.getMat().size;
    const int src_c = src_size[0];
    const int src_h = src_size[1];
    const int src_w = src_size[2];

    auto c_hw = src.getMat().reshape(0, { src_c, src_h * src_w });

    dst.create(src_h, src_w, CV_MAKETYPE(src.depth(), src_c));
    cv::Mat dst_1d = dst.getMat().reshape(src_c, { src_h, src_w });

    cv::transpose(c_hw, dst_1d);
}

static void hwc_to_chw(const uchar* input, size_t h, size_t w, float** output, size_t* output_count)
{
    size_t stride = h * w;
    *output_count = stride * 3;
    float* output_data = (float*)malloc(*output_count * sizeof(float));
    for (size_t i = 0; i != stride; ++i) {
        for (size_t c = 0; c != 3; ++c) {

            output_data[c * stride + i] = input[i * 3 + c];
            //output_data[c * stride + i] = (float)(input[i * 3 + c] * (2. / 255.0) - 1.0); // -1 ~ 1
            //output_data[c * stride + i] = (float)(input[i * 3 + c] * (1. / 255.0));
        }
    }

    *output = output_data;
}
static void hwc_to_chw(const float* input, size_t h, size_t w, float** output, size_t* output_count)
{
    size_t stride = h * w;
    *output_count = stride * 3;
    float* output_data = (float*)malloc(*output_count * sizeof(float));
    for (size_t i = 0; i != stride; ++i) {
        for (size_t c = 0; c != 3; ++c) {

            output_data[c * stride + i] = input[i * 3 + c];
            //output_data[c * stride + i] = (float)(input[i * 3 + c] * (2. / 255.0) - 1.0); // -1 ~ 1
            //output_data[c * stride + i] = (float)(input[i * 3 + c] * (1. / 255.0));
        }
    }
    *output = output_data;
}
static void hwc_to_cwh(const uchar* input, const size_t H, const size_t W, const size_t C, float** output, size_t* output_count)
{
    size_t count = H * W * C;
    //float* data = (float*)malloc(count * sizeof(float));
    float* data = new float[count];
    for (size_t hi = 0; hi < H; hi++)
    {
        for (size_t wi = 0; wi < W; wi++)
        {
            for (size_t ci = 0; ci < C; ci++)
            {
                size_t mat_idx = (hi * C * W) + (wi * C) + ci;      //h w c
                size_t tensor_idx = (ci * W * H) + (wi * H) + hi;   //c w h

                data[tensor_idx] = (float)(input[mat_idx] * (2. / 255.0) - 1.0); // -1 ~ 1
            }
        }
    }
    *output = data;
    *output_count = count;
}

/*
* Needs to refactoring 
* ; require param on layout of tensor. (current layout of tensor is assumed to 'NCWH'.)
* ; need to data type.
*/

static void ReleaseTensors(std::vector<const Ort::Value*> tensors)
{
    for (auto t : tensors)
        ReleaseTensor(t);
}
static void ReleaseTensor(const Ort::Value* tensor)
{
    const float* data = tensor->GetTensorData<float>();
    delete data;
}