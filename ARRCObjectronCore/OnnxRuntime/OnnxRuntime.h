#pragma once
#include <assert.h>
#include <stdio.h>
#include <vector>
#include <string>
#include <onnxruntime_cxx_api.h>
#include <algorithm>
#include <numeric>
#include <Windows.h>

#ifdef USE_WIN_ML
#include "dml_provider_factory.h"
#endif

typedef struct __NodeProperty
{
    const char* name;
    std::vector<int64_t> shape;
    ONNXTensorElementDataType type;
    size_t elementsCount;

}NodeProperty;

class OnnxRuntime
{
    //const OrtApi* g_ort;
    Ort::Env* env = nullptr;
    Ort::Session* session = nullptr;
    Ort::SessionOptions session_options;
    

    
public:
    OnnxRuntime()
    {
        env = new Ort::Env(ORT_LOGGING_LEVEL_WARNING, "test");
        session_options.SetIntraOpNumThreads(1);
    }
    ~OnnxRuntime()
    {
        session->release();
        env->release();
    }
    //void initialize_onnx_runtime();

    void LoadModel(const char* model_path);
    std::vector<NodeProperty> GetInputLayout();
    std::vector<NodeProperty> GetOutputLayout();

    std::vector<Ort::Value> Evaluate(std::vector<const Ort::Value*> input_tensors);

    std::vector<NodeProperty> m_inputNodes;
    std::vector<NodeProperty> m_outputNodes;
    std::vector<const char*> m_inputNodeNames;
    std::vector<const char*> m_outputNodeNames;

};


