//#include "pch.h"
#include "OnnxRuntime.h"

//#include "cuda_provider_factory.h"
//#include "dml_provider_factory.h"

static std::wstring s2ws(const std::string& str);

void OnnxRuntime::LoadModel(const char* model_path)
{
    //*************************************************************************
    // create session and load model into memory
    // using squeezenet version 1.3
    // URL = https://github.com/onnx/models/tree/master/squeezenet

    //#ifdef _WIN32
    
    std::wstring wModelPath = s2ws(model_path); //L"e:/models/object_detection_3d_sneakers.onnx";

    printf("Using Onnxruntime C API\n");
    session = new Ort::Session(*env, wModelPath.c_str(), session_options);

    m_inputNodes = GetInputLayout();
    m_outputNodes = GetOutputLayout();

    //이름만 따로 모음.
    std::vector<const char*> inputNodeNames;
    for_each(m_inputNodes.begin(), m_inputNodes.end(),
        [&inputNodeNames](NodeProperty& p) {inputNodeNames.push_back(p.name); });

    std::vector<const char*> outputNodeNames;
    for_each(m_outputNodes.begin(), m_outputNodes.end(),
        [&outputNodeNames](NodeProperty& p) {outputNodeNames.push_back(p.name); });

    m_inputNodeNames = inputNodeNames;
    m_outputNodeNames = outputNodeNames;
}

std::vector<NodeProperty> OnnxRuntime::GetInputLayout()
{
    std::vector<NodeProperty> out_nodeProperty;
    size_t num_input_nodes = session->GetInputCount();

    Ort::AllocatorWithDefaultOptions allocator;
    //printf("Number of input nodes = %zu\n", num_input_nodes);
    
    // iterate over all input nodes
    for (size_t i = 0; i < num_input_nodes; i++)
    {
        NodeProperty nodeProperty;
        // print input node names
        char* input_name = session->GetInputName(i, allocator);
        //printf("Input %zu : name=%s\n", i, input_name);
        nodeProperty.name = input_name;

        // print input node types
        OrtTypeInfo* typeinfo;
        Ort::TypeInfo type_info = session->GetInputTypeInfo(i);
        auto tensor_info = type_info.GetTensorTypeAndShapeInfo();
        
        ONNXTensorElementDataType type = tensor_info.GetElementType();
        //printf("Input %d : type=%d\n", i, type);
        nodeProperty.type = type;

        // print input shapes/dims
        std::vector<int64_t> input_node_dims = tensor_info.GetShape();
        
        //printf("Input %d : num_dims=%zu\n", i, input_node_dims.size());

        //for (int j = 0; j < input_node_dims.size(); j++)
            //printf("Input %d : dim %d=%jd\n", i, j, input_node_dims[j]);
        
        nodeProperty.shape = input_node_dims;
        nodeProperty.elementsCount = tensor_info.GetElementCount();
        out_nodeProperty.push_back(nodeProperty);
    }

    return out_nodeProperty;
}
std::vector<NodeProperty> OnnxRuntime::GetOutputLayout()
{
    std::vector<NodeProperty> out_nodeProperty;
    size_t num_output_nodes = session->GetOutputCount();

    Ort::AllocatorWithDefaultOptions allocator;
    //printf("Number of output nodes = %zu\n", num_output_nodes);

    // iterate over all input nodes
    for (size_t i = 0; i < num_output_nodes; i++)
    {
        NodeProperty nodeProperty;
        // print input node names
        char* input_name = session->GetOutputName(i, allocator);
        //printf("Output %zu : name=%s\n", i, input_name);
        nodeProperty.name = input_name;

        // print input node types
        OrtTypeInfo* typeinfo;
        Ort::TypeInfo type_info = session->GetOutputTypeInfo(i);
        auto tensor_info = type_info.GetTensorTypeAndShapeInfo();

        ONNXTensorElementDataType type = tensor_info.GetElementType();
        //printf("Input %d : type=%d\n", i, type);
        nodeProperty.type = type;

        // print input shapes/dims
        std::vector<int64_t> input_node_dims = tensor_info.GetShape();
        //printf("Input %d : num_dims=%zu\n", i, input_node_dims.size());
        //for (int j = 0; j < input_node_dims.size(); j++)
            //printf("Input %d : dim %d=%jd\n", i, j, input_node_dims[j]);
        nodeProperty.shape = input_node_dims;

        out_nodeProperty.push_back(nodeProperty);
    }
    return out_nodeProperty;
}

std::vector<Ort::Value> OnnxRuntime::Evaluate(std::vector<const Ort::Value*> input_tensors)
{
    //Validate layout of input tensors.
    assert(input_tensors.size() == m_inputNodes.size() && "Incorrect number of tensors.");
    
    for(size_t i=0; i< input_tensors.size(); i++)
    {
        auto info = input_tensors[i]->GetTensorTypeAndShapeInfo();
        auto shape = info.GetShape();
        auto type = info.GetElementType();
        auto count = info.GetElementCount();

        assert(count == m_inputNodes[i].elementsCount && "Incorrect element count of the tensor.");

        assert(std::equal(shape.begin(), shape.end(), m_inputNodes[i].shape.begin()) 
            && "Incorrect shape of tensor");

        assert(type == m_inputNodes[i].type && "Incorrect type of tensor");
    }

    return session->Run(Ort::RunOptions{ nullptr }, m_inputNodeNames.data(), 
        input_tensors[0], input_tensors.size(), m_outputNodeNames.data(), m_outputNodeNames.size());
}


static std::wstring s2ws(const std::string& str)
{
    int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
    std::wstring wstrTo(size_needed, 0);
    MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
    return wstrTo;
}


