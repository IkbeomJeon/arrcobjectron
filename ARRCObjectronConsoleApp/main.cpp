#include <ctime>
#include <iostream>
#include <fstream>
#include <opencv2/highgui.hpp>
#include <opencv2/core.hpp>


#include "../ARRCObjectronCore/OnnxRuntime/OnnxRuntime.h"
#include "../ARRCObjectronCore/OnnxRuntime/OnnxRuntimeUtils.h"
#include "../ARRCObjectronCore/Utils/CVUtils.h"
#include "../ARRCObjectronCore/ARRCObjectronCore/BoxUtil.h"
#include "../ARRCObjectronCore/ARRCObjectronCore/Tensors_to_objects_calculator.h"
#include "../ARRCObjectronCore/ARRCObjectronCore/annotation_to_model_calculator.h"
using namespace ARRCObjectron;

//using native onnx runtime.

cv::Mat ResizeAndCrop(cv::Mat image, bool isInputTranspose, int width, int height);
std::vector<Ort::Value> Eval(cv::Mat image, OnnxRuntime& onnx, std::string resultFilePath);
void VisualizeResult(cv::Mat rgb_image, std::vector<Ort::Value>& output_tensors);

int main(int argc, char* argv[])
{
    OnnxRuntime onnx;
    const char* modelpath;
    std::string inputStream;
    bool isDebug = (std::string(argv[argc - 1]) == "debug");
    std::vector<Ort::Value> output_tensors;

    inputStream = std::string(argv[2]);

    if (isDebug)
    {
        isDebug = true;
        modelpath = "D:/models/Virnect_23456/000114.onnx"; //23456
        //modelpath = "D:/models/Virnect/obj5_mix/105_train.onnx"; // obj5 car
        //modelpath = "D:/models/Virnect_obj3/21.onnx"; //obj3
        //modelpath = "D:/models/lmo_204.onnx"; //lmo
        //modelpath = "D:/models/tless_obj23_23.onnx"; //tless obj23
    }
    else
    {
        modelpath = argv[1];
    }


    onnx.LoadModel(modelpath);

#pragma region cameraInput
    if(inputStream == "cam")
    {
        bool bQuit = false;

        cv::Mat image;
        std::string resultFilePath = std::string("./result_pose_list.csv");

        cv::VideoCapture* cap = new cv::VideoCapture(0);

        if (!cap->isOpened())  // check if succeeded to connect to the camera
            CV_Assert("Cam open failed");

        cap->set(cv::CAP_PROP_FRAME_WIDTH, 640);
        cap->set(cv::CAP_PROP_FRAME_HEIGHT, 480);

        while (1)
        {
            *cap >> image;

            cv::Mat crop = ResizeAndCrop(image, !isDebug, 640, 480);
            output_tensors = Eval(crop, onnx, resultFilePath);
            VisualizeResult(crop, output_tensors);

            char key = cv::waitKey(1);

            if (key == 'q' || key == 'Q')
                break;
        }
        //std::cout <<  EXIT." << std::endl;
        delete cap;

    }
#pragma endregion
    else
    {
        std::string dirpath = std::string(inputStream.substr(0, inputStream.find_last_of("\\")));

        std::string const file_ext = inputStream.substr(inputStream.find_last_of(".") + 1);

        std::string resultFilePath = dirpath + std::string("\\result_pose_list.csv");

#pragma region videoInput
        if (file_ext == "MOV" || file_ext == "mov" || file_ext == "MP4"
            || file_ext == "mp4" || file_ext == "AVI" || file_ext == "avi")
        {
            cv::Mat image;
            auto cap = cv::VideoCapture(inputStream);
            bool bQuit = false;

            for (int frame_count = 0; cap >> image, cap.isOpened(); frame_count++)
            {
                cv::Mat crop = ResizeAndCrop(image, !isDebug, 640, 480);
                output_tensors = Eval(crop, onnx, resultFilePath);
                VisualizeResult(crop, output_tensors);

                char key = cv::waitKey(atoi(argv[4]));

                if (key == 'q' || key == 'Q')
                    break;
            }

        }
#pragma endregion
        else if (file_ext == "jpg" || "png")
        {
            char filepath[256];
            for (int frame_count = 0; ; frame_count++)
            {

                //sprintf_s(filepath, "D:/dataset/BOP/Virnect/obj5_all_t argets/000014/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "D:/dataset/BOP/Virnect/test_real/20210810_sporty_car/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "X:/CAD_BOP/test/000082/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "X:/CAD_BOP/test_real/20210810_hollow_cube/rgb/%06d.png", frame_count);
                sprintf_s(filepath, "X:/CAD_BOP/test_real/20210810_Plane/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "X:/CAD_BOP/test/000001/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "D:/dataset/BOP/tless/test_primesense/000008/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "D:/dataset/BOP/lm/test/000001/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "D:/dataset/BOP/lmo/test/000002/rgb/%06d.png", frame_count);
                //sprintf_s(filepath, "D:/dataset/BOP/lm/train_pbr/000002/rgb/%06d.jpg", frame_count);
                //sprintf_s(filepath, "D:/dataset/BOP/lm/train_pbr/000002/rgb/%06d.jpg", frame_count);
                cv::Mat image = cv::imread(std::string(filepath));
                //image = ConvertImage(image, !isDebug);
                //cv::Mat crop = ResizeAndCrop(image, !isDebug);
                cv::Mat resized;
                cv::resize(image, resized, cv::Size(640, 480));
                output_tensors = Eval(resized, onnx, resultFilePath);
                VisualizeResult(image, output_tensors);
                char key = cv::waitKey(1);
            }


        }

    }

  
    return 0;
}

void WriteAnnotation(std::string writefilepath, int frame_count, float fps, ARRCObjectron::FrameAnnotation frame_annotation)
{
    // write File
    std::ofstream writeFile(writefilepath, std::fstream::out | std::fstream::app);

    if (writeFile.is_open()) {
        writeFile << "\n";
        writeFile << frame_count << ",";
        auto& annotations = frame_annotation.annotations();

        int num_objects = annotations.size();
        for (const auto& ann : annotations)
        {
            const auto pKeypoints = ann.get()->keypoints(); //const

            std::vector<cv::Point> points_pixel;

            for_each(pKeypoints.begin(), pKeypoints.end(),
                [&points_pixel, &writeFile](const std::shared_ptr< ARRCObjectron::AnnotatedKeyPoint> pkp)
                {
                    float x = pkp->point_3d().x;
                    float y = pkp->point_3d().y;

                    points_pixel.push_back(cv::Point(x, y));
                    writeFile << x << "," << y << ',';
                }
            );
            writeFile << fps;
        }

        writeFile.close();
    }
}

std::vector<Ort::Value> Eval(cv::Mat image, OnnxRuntime& onnx, std::string resultFilePath)
{
    //8U -> 32F, [0,255] -> [-1, 1]
    cv::Mat img_norm;
    image.convertTo(img_norm, CV_32F, 2/255.0, -1.0);

    /* Create input tensors.*/
    clock_t start_time = std::clock();
    Ort::Value tensor = CreateCHWTensor_float32(img_norm); //warning : the data of input tensors is manually allocated, so it is needed to release manually.
    std::vector<const Ort::Value*> input_tensors = { &tensor };
    std::cout << "Create tensor time : " << std::clock() - start_time << std::endl;

    /* Evaluation */
    start_time = std::clock();
    auto output_tensors = onnx.Evaluate(input_tensors);
    ReleaseTensors(input_tensors); 
    clock_t eval_time = (std::clock() - start_time);
    std::cout << resultFilePath << ", Evaluation time : " << eval_time << ", FPS : " << 1000.f / eval_time << std::endl;

    return output_tensors;
}

cv::Mat ResizeAndCrop(cv::Mat image, bool isInputTransposed, int width, int height)
{
    clock_t start_time = std::clock();

    cv::Size image_size(640, 480);
    cv::Mat crop = Crop(image, image_size.width, image_size.height);

    cv::Mat resized;
    cv::resize(crop, resized, cv::Size(width, height));

    //cvtColor(crop, crop, cv::COLOR_BGR2RGB);

    //hwc -> whc
    if (isInputTransposed)
        resized = Transpose(resized);


    std::cout << "Crop, resize : " << std::clock() - start_time << std::endl;
    return resized;
}

void VisualizeResult(cv::Mat rgb_image, std::vector<Ort::Value>& output_tensors)
{
    clock_t start_time = std::clock();
    auto probmap = CreateMat_from_CHWTensor_float32(&output_tensors[0]);

    auto offssetmap = CreateMat_from_CHWTensor_float32(&output_tensors[1]);
    //auto mask = Tensor2Mat(&output_tensors[2]);
    std::cout << "Tensor to Mat time : " << std::clock() - start_time << std::endl;

    /* Process */
    cv::Mat result = rgb_image.clone();

    auto tensor_to_annotation = new TensorsToObjectsCalculator(
        TensorsToObjectsCalculatorOptions(BeliefDecoderConfig()));
    auto frame_annotation = tensor_to_annotation->Process(probmap, offssetmap);

    //auto annottation_to_model = new AnnotationsToModelMatricesCalculator(Eigen::Vector3f(1.f, 1.f, 1.f));
    //auto model_matrix_list = annottation_to_model->Process(frame_annotation);

    //Eigen::Matrix<float, 4, 4, Eigen::RowMajor> projection_matrix_;

    //projection_matrix_ <<
    //    1.5731, 0, 0, 0,
    //    0, 2.0975, 0, 0,
    //    0, 0, -1.0002, -0.2,
    //    0, 0, -1, 0;
    //
    //Eigen::Matrix<float, 4, 4, Eigen::RowMajor> projection_matrix_2;
    //projection_matrix_2 <<
    //    572.4114, 0, 325.2611, 0,
    //    0, 573.57043, 242.04899, 0,
    //    0, 0, 1, 0,
    //    0, 0, 0, 1;

    //std::vector<Eigen::Vector4f> bboxes;

    //bboxes.push_back(Vector4f(-49.761f, -17.45001f, -53.715f, 1));
    //bboxes.push_back(Vector4f(-49.761f, -17.45001f,  53.715f, 1));
    //bboxes.push_back(Vector4f(-49.761f,  19.32902,  -53.715f, 1));
    //bboxes.push_back(Vector4f(-49.761f,  19.32902f,  53.715f, 1));
    //bboxes.push_back(Vector4f(51.259f, -17.45001f, -53.715f, 1));
    //bboxes.push_back(Vector4f(51.259f, - 17.45001f,  53.715f, 1));
    //bboxes.push_back(Vector4f(51.259f,   19.32902f, -53.715f, 1));
    //bboxes.push_back(Vector4f(51.259f,    19.32902f,  53.715f, 1));

    //auto model_matrix_proto = model_matrix_list->model_matrix_proto();
    //auto model_matrix =  model_matrix_proto[0]->model_matrix();

    //for (auto point3d : bboxes)
    //{
    //    Eigen::Vector4f t3d = model_matrix * point3d;
    //    

    //       float u, v;
    //       //Eigen::Vector4f point3d_projection = projection_matrix_2 * model_matrix * point3d;
    //       //const float inv_w = 1.0f / point3d_projection(2);
    //       //u = point3d_projection(0) * inv_w;
    //       //v = point3d_projection(1) * inv_w;

    //       Eigen::Vector4f point3d_projection = projection_matrix_ * model_matrix * point3d;
    //       const float inv_w = 1.0f / point3d_projection(3);
    //       u = (point3d_projection(0) * inv_w + 1.0f) * 0.5f;
    //       v = (1.0f - point3d_projection(1) * inv_w) * 0.5f;
    //       int a = v;
    //       //key_point->mutable_point_2d()->x = u;
    //       //key_point->mutable_point_2d()->y = v;
    //}

    //for (auto ann : frame_annotation->mutable_annotations()) {
    //    for (auto key_point : ann->mutable_keypoints()) {
    //        Eigen::Vector4f point3d;

    //        point3d << key_point->point_3d().x, key_point->point_3d().y, key_point->point_3d().z, 1.0f;
    //        Eigen::Vector4f point3d_projection = projection_matrix_ * model_matrix * point3d;

    //        float u, v;
    //        const float inv_w = 1.0f / point3d_projection(3);
    //        //if (portrait) {
    //            u = (point3d_projection(1) * inv_w + 1.0f) * 0.5f;
    //            v = (point3d_projection(0) * inv_w + 1.0f) * 0.5f;
    //        /*}
    //        else {
    //            u = (point3d_projection(0) * inv_w + 1.0f) * 0.5f;
    //            v = (1.0f - point3d_projection(1) * inv_w) * 0.5f;
    //        }*/
    //        /* u = 640-point3d_projection(0) * inv_w;
    //         v = point3d_projection(1) * inv_w;*/

    //        key_point->mutable_point_2d()->x = u;
    //        key_point->mutable_point_2d()->y = v;

    //    }
    //}
    
    DrawAnnotations(result, *frame_annotation);

    //WriteAnnotation(resultFilePath, frame_count, fps, *frame_annotation);

    //auto mask_t = Transpose(mask);
    //auto result_t = Transpose(result);

    /* Show results */
    auto heatmap = ConvertToHeatMap(probmap);
    cv::Mat overay_heatmap, overay_mask, overay_result;
    resize(heatmap, heatmap, rgb_image.size(), 0, 0, cv::InterpolationFlags::INTER_NEAREST);

    //auto mask_uc3 = ConvertFloatMatToUC3(mask);
    //cv::cvtColor(mask_uc3, mask_uc3, cv::COLOR_RGBA2RGB);
    //resize(mask_uc3, mask_uc3, crop_t.size(), 0, 0, cv::InterpolationFlags::INTER_NEAREST);
    //cv::addWeighted(crop_t, 0.3, mask_uc3, 0.7, 0, overay_mask);
    //cv::moveWindow("overay_mask", crop_t.cols, 0);
    //imshow("overay_mask", overay_mask);
    cv::addWeighted(rgb_image, 0.5, heatmap, 0.5, 0, overay_heatmap);
    cv::addWeighted(rgb_image, 0.3, result, 0.7, 0, overay_result);

    imshow("overay_heatmap", overay_heatmap);

    imshow("result", overay_result);
    cv::moveWindow("overay_heatmap", 0, 0);

    cv::moveWindow("result", rgb_image.cols * 1, 0);
}

