# ARRCObjectron

### Contents

- ARRCObjectronCore (c++ , shared I)

  물체 검출,자세 추정과 관련된 코어 라이브러리.
  

- ARRCObjectronConsoleApp

  물체 검출 라이브러리 테스트용 콘솔 프로그램.

  

- UnitTests

  - ARRCObjectronCore 에 구현된 모듈들의 unittest용 프로젝트.

### Acknowledgment
This work was supported by Institute of Information & communications Technology Planning & Evaluation (IITP) grant funded by the Korea government(MSIT) (No.2019-0-01270, WISE AR UI/UX Platform Development for Smartglasses)


